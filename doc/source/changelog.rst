.. This file is part of the 'astrophysix' Python package documentation.
   -----------------------------------------------------------------------------------
   Copyright © Commissariat a l'Energie Atomique et aux Energies Alternatives (CEA)
   -----------------------------------------------------------------------------------
   -----------------------
   FREE SOFTWARE LICENCING
   -----------------------
   This software is governed by the CeCILL license under French law and abiding by the rules of distribution of free
   software. You can use, modify and/or redistribute the software under the terms of the CeCILL license as circulated by
   CEA, CNRS and INRIA at the following URL: "http://www.cecill.info". As a counterpart to the access to the source code
   and rights to copy, modify and redistribute granted by the license, users are provided only with a limited warranty
   and the software's author, the holder of the economic rights, and the successive licensors have only limited
   liability. In this respect, the user's attention is drawn to the risks associated with loading, using, modifying
   and/or developing or reproducing the software by the user in light of its specific status of free software, that may
   mean that it is complicated to manipulate, and that also therefore means that it is reserved for developers and
   experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the
   software's suitability as regards their requirements in conditions enabling the security of their systems and/or data
   to be ensured and, more generally, to use and operate it in the same conditions as regards security. The fact that
   you are presently reading this means that you have had knowledge of the CeCILL license and that you accept its terms.
   -----------------------
   COMMERCIAL SOFTWARE LICENCING
   -----------------------------
   You can obtain this software from CEA under other licencing terms for commercial purposes. For this you will need to
   negotiate a specific contract with a legal representative of CEA.

Changelog
=========


0.6.0 (in prep.)
----------------

in ``astrophysix.simdm`` package :

  * Added new :func:`ObjectList.clear <astrophysix.simdm.utils.ObjectList.clear>` method,
  * Added new :attr:`FileType.YAML_FILE <astrophysix.utils.file.FileType.YAML_FILE>` file type and
    :class:`~astrophysix.simdm.datafiles.file.YamlFile` class,
  * Added new :attr:`~astrophysix.simdm.project.ProjectCatagory.StellarEnvironments`
    :class:`~astrophysix.simdm.project.ProjectCatagory`,
  * Added :attr:`~astrophysix.simdm.experiment.base.Experiment.configuration_file` property to store any
    :class:`~astrophysix.simdm.Experiment.Simulation` or :class:`~astrophysix.simdm.experiment.PostProcessingRun`
    parameters into an Ascii, Json, or Yaml formatted file,
  * Snapshot/Catalog terminus service binding enabled : added :class:`~astrophysix.simdm.services.process.DataProcessingService`,
    :class:`~astrophysix.simdm.services.process.CatalogDataProcessingService` and
    :class:`~astrophysix.simdm.services.process.CatalogFieldBinding` classes to define data processing services, bound
    to a :class:`~astrophysix.simdm.results.snapshot.Snapshot` or an object :class:`~astrophysix.simdm.catalogs.catalog.Catalog`.


0.5.0 (Feb. 11th, 2021)
-----------------------

in ``astrophysix.simdm`` package :

  * Added :class:`~astrophysix.simdm.catalogs.targobj.TargetObject`,
    :class:`~astrophysix.simdm.catalogs.targobj.ObjectProperty`,
    :class:`~astrophysix.simdm.catalogs.targobj.ObjectPropertyGroup` classes,
    :class:`~astrophysix.simdm.catalogs.targobj.PropertySortFlag` and
    :class:`~astrophysix.simdm.catalogs.targobj.PropertyFilterFlag` enums to describe catalog objects,
  * Added :class:`~astrophysix.simdm.catalogs.catalog.Catalog` and
    :class:`~astrophysix.simdm.catalogs.field.CatalogField` classes defining object catalogs,
  * added :attr:`Project.acknowledgement <astrophysix.simdm.Project.acknowledgement>` property,
  * Added :class:`~astrophysix.simdm.protocol.Physics` :

    + :attr:`Physics.ExternalGravity <astrophysix.simdm.protocol.Physics.ExternalGravity>`,
    + :attr:`Physics.SupermassiveBlackHoleFeedback <astrophysix.simdm.protocol.Physics.SupermassiveBlackHoleFeedback>`,
    + :attr:`Physics.StellarIonisingRadiation <astrophysix.simdm.protocol.Physics.StellarIonisingRadiation>`,
    + :attr:`Physics.StellarUltravioletRadiation <astrophysix.simdm.protocol.Physics.ExternalGravity>`,
    + :attr:`Physics.StellarInfraredRadiation <astrophysix.simdm.protocol.Physics.StellarInfraredRadiation>`,
    + :attr:`Physics.ProtostellarJetFeedback <astrophysix.simdm.protocol.Physics.ExternalGravity>`,
    + :attr:`Physics.ExternalGravity <astrophysix.simdm.protocol.Physics.ProtostellarJetFeedback>`,
    + :attr:`Physics.DustCooling <astrophysix.simdm.protocol.Physics.DustCooling>`,
    + :attr:`Physics.MolecularCooling <astrophysix.simdm.protocol.Physics.MolecularCooling>`,
    + :attr:`Physics.AtomicCooling <astrophysix.simdm.protocol.Physics.AtomicCooling>`,
    + :attr:`Physics.TurbulentForcing <astrophysix.simdm.protocol.Physics.TurbulentForcing>`,
    + :attr:`Physics.RadiativeTransfer <astrophysix.simdm.protocol.Physics.RadiativeTransfer>`.

  * Added :class:`~astrophysix.simdm.protocol.AlgoType` :

    + :attr:`AlgoType.RadiativeTransfer <astrophysix.simdm.protocol.AlgoType.RadiativeTransfer>`,
    + :attr:`AlgoType.RungeKutta <astrophysix.simdm.protocol.AlgoType.RungeKutta>`,
    + :attr:`AlgoType.StructuredGrid <astrophysix.simdm.protocol.AlgoType.StructuredGrid>`,
    + :attr:`AlgoType.SpectralMethod <astrophysix.simdm.protocol.AlgoType.SpectralMethod>`,
    + :attr:`AlgoType.NBody <astrophysix.simdm.protocol.AlgoType.NBody>`.

  * :class:`~astrophysix.simdm.datafiles.file.AssociatedFile` persistency bug fix,
  * ``none`` :class:`~astrophysix.units.unit.Unit` persistency bug fix,


0.4.2 (Dec. 8th, 2020)
----------------------

  * Galactica validation fully operational.


0.4.1 (Oct. 16th, 2020)
-----------------------
  * Set :attr:`InputParameter.name <astrophysix.simdm.protocol.InputParameter.name>` property as index in `Protocol` input
    parameter list. Set :attr:`InputParameter.key <astrophysix.simdm.protocol.InputParameter.key>` as optional
    (:galactica:`Galactica simulation database <>` compatibility).


0.4.0 (Oct. 14th, 2020)
-----------------------

Implemented ``Simulation Datamodel`` classes :

 * :class:`~astrophysix.simdm.Project` and :class:`~astrophysix.simdm.SimulationStudy`,
 * `Protocols` (:class:`~astrophysix.simdm.protocol.SimulationCode` and
   :class:`~astrophysix.simdm.protocol.PostProcessingCode`),
 * `Experiments` (:class:`~astrophysix.simdm.Experiment.Simulation` and :class:`~astrophysix.simdm.experiment.PostProcessingRun`),
 * `Results` (:class:`~astrophysix.simdm.results.generic.GenericResult` and :class:`~astrophysix.simdm.results.snapshot.Snapshot`),
 * :class:`~astrophysix.simdm.datafiles.Datafile`.