.. Astrophysix documentation master file, created by sphinx-quickstart on Mon Jun  3 10:02:05 2019.
   This file is part of the 'astrophysix' Python package.
   -----------------------------------------------------------------------------------
   Copyright © Commissariat a l'Energie Atomique et aux Energies Alternatives (CEA)
   -----------------------------------------------------------------------------------
   -----------------------
   FREE SOFTWARE LICENCING
   -----------------------
   This software is governed by the CeCILL license under French law and abiding by the rules of distribution of free
   software. You can use, modify and/or redistribute the software under the terms of the CeCILL license as circulated by
   CEA, CNRS and INRIA at the following URL: "http://www.cecill.info". As a counterpart to the access to the source code
   and rights to copy, modify and redistribute granted by the license, users are provided only with a limited warranty
   and the software's author, the holder of the economic rights, and the successive licensors have only limited
   liability. In this respect, the user's attention is drawn to the risks associated with loading, using, modifying
   and/or developing or reproducing the software by the user in light of its specific status of free software, that may
   mean that it is complicated to manipulate, and that also therefore means that it is reserved for developers and
   experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the
   software's suitability as regards their requirements in conditions enabling the security of their systems and/or data
   to be ensured and, more generally, to use and operate it in the same conditions as regards security. The fact that
   you are presently reading this means that you have had knowledge of the CeCILL license and that you accept its terms.
   -----------------------
   COMMERCIAL SOFTWARE LICENCING
   -----------------------------
   You can obtain this software from CEA under other licencing terms for commercial purposes. For this you will need to
   negotiate a specific contract with a legal representative of CEA.


Astrophysix package documentation
=================================

.. image:: https://img.shields.io/pypi/v/astrophysix?color=red&logo=PyPI
   :alt: PyPI - Version

.. image:: https://img.shields.io/pypi/status/astrophysix?color=orange&logo=PyPI
   :alt: PyPI - Status

.. image:: https://img.shields.io/pypi/pyversions/astrophysix?color=green&logo=PyPI
   :alt: PyPI - Python Version

.. image:: https://img.shields.io/readthedocs/astrophysix?logo=read-the-docs&logoColor=white
   :target: https://astrophysix.readthedocs.io/en/latest/?badge=latest
   :alt: Read the Docs - Documentation Status

.. image:: https://img.shields.io/pypi/wheel/astrophysix?label=wheel&color=yellowgreen&logo=PyPI
   :alt: PyPI - Wheel

.. image:: https://img.shields.io/pypi/l/astrophysix?color=informational&logo=PyPI
   :alt: PyPI - License


Introduction
------------

The purpose of the ``astrophysix`` Python package is to provide computational astrophysicists a generic tool to document
their numerical projects, **independently** of :

   - the astrophysical code they used (RAMSES, AREPO, GADGET, ASH, PLUTO, MAGICS, etc),
   - the hardware/software parameters of their numerical setup,
   - the post-processing software/library they perform their scientific analysis with (Yt, Osyris, PyMSES, RADMC-3D,
     SAODS9, Paraview, etc.),
   - the type of their scientific analysis (2D column density maps, 3D power spectrum, PPV cubes, statistical analysis,
     etc.).

``astrophysix`` contains :

* the ``astrophysix.simdm`` package follows the `Simulation Datamodel` (`SimDM documentation`_) standard documented by the
  `International Virtual Observatory Alliance`_.

.. image:: _static/img/IVOA.jpg
   :align: center
   :width: 150 px
   :target: `International Virtual Observatory Alliance`_

* the ``astrophysix.units`` package provides a more generic-purpose physical quantity and units management tool. The
  most common physical constants used in astrophysics are defined in this module.

With this package, users can create a :class:`~astrophysix.simdm.SimulationStudy` in which a single numerical
project can be fully documented and :

 - all the reduced datasets (PNG plots, FITS files, etc.) and the experimental configuration files (e.g. RAMSES namelist)
   can be attached as "binary attachments",
 - complete catalogs of objects identified in the simulations can be associated to the numerical experiment's results.

It follows the hierarchical structure :

.. image:: _static/img/simdm_hierarchy.png


Galactica database integration
------------------------------

.. figure:: _static/img/galactica_preview.png
   :target: http://www.galactica-simulations.eu
   :align: center

   `http://www.galactica-simulations.eu <http://www.galactica-simulations.eu>`_


These studies can be saved in persistent and portable HDF5 files and then distributed to other members of the scientific
collaboration to be updated. :class:`~astrophysix.simdm.SimulationStudy` HDF5 files can be uploaded with a
single-click on the :galactica:`Galactica simulation database <>` to automatically deploy web pages for your
astrophysical simulation project.

:class:`~astrophysix.simdm.SimulationStudy` HDF5 files can be uploaded on the :galactica:`Galactica simulation database <>`
several times in order to :

* Create new project web pages on the web application (creation),
* Update pages for an existing project (update).

.. warning::

   When you upload a :class:`~astrophysix.simdm.SimulationStudy` HDF5 file, the :galactica:`Galactica server <>` will
   **NEVER** take the responsibility of deleting any entry from the database related to an item that
   is missing in your :class:`~astrophysix.simdm.SimulationStudy` HDF5 file. In other words, deleting an object from your
   local :class:`~astrophysix.simdm.SimulationStudy`, saving the study into a HDF5 file and uploading the file on
   :galactica:`Galactica <>` won't delete the object from the database (only the `creation` and `update` behaviours are
   enabled).

   If you wish to remove items from the web application, you **MUST** do so by hand (it must be an explicit user action)
   in the :galactica:`Galactica administration interface <>`.

Galactica validity checks
^^^^^^^^^^^^^^^^^^^^^^^^^

Prior to uploading any Simulation study HDF5 file on the :galactica:`Galactica web application <>`, it is advised to
perform some preliminary validity checks on your project to make sure the deployment online will go smoothly. To do so,
you may use the :func:`~astrophysix.simdm.Project.galactica_validity_check()` method or enable the ``galactica_checks`` option
in the :func:`SimulationStudy.save_HDF5() <astrophysix.simdm.SimulationStudy.save_HDF5>` method. For more details, see
:ref:`check_galactica_validation`


.. _International Virtual Observatory Alliance: http://ivoa.net/
.. _SimDM documentation: https://wiki.ivoa.net/internal/IVOA/IVOATheorySimDMspec/PR-SimulationDataModel-v.1.00-20111019.pdf


Online data-processing services (new in version 0.6.0)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The :galactica:`Galactica web application <>` also features online data-processing services through a web form.
Authenticated users have the possibility to submit job requests directly on the platform to compute and retrieve
post-processed datasets directly, obtained from the raw simulation data :

.. image:: _static/img/job_request_form_galactica.png
   :align: center
   :width: 700 px
   :target: http://www.galactica-simulations.eu

These job requests are then routed to an ecosystem of data-processing servers (aka `Terminus`_ servers) hosting the
raw simulation data and the required post-processing libraries.
Using the ``astrophysix`` package, you have the possibility to define into your
:class:`~astrophysix.simdm.SimulationStudy` which data-processing service you want to bind to a simulation's
:class:`~astrophysix.simdm.results.snapshot.Snapshot` or even a
:class:`~astrophysix.simdm.catalogs.catalog.Catalog`. For more details, see :ref:`processing_services`.


.. _Terminus: https://galactica-terminus.readthedocs.io/en/latest/


.. toctree::
   :maxdepth: 3
   :hidden:
   :caption: Documentation

   usage/installation
   usage/quickstart
   usage/codes
   usage/runs
   usage/results
   usage/catalogs
   usage/process_services
   usage/units
   faq
   changelog



.. toctree::
   :maxdepth: 3
   :hidden:
   :caption: SimDM API reference

   api_ref/simdm/proj_study
   api_ref/simdm/protocols
   api_ref/simdm/experiments
   api_ref/simdm/results
   api_ref/simdm/catalogs
   api_ref/simdm/services
   api_ref/simdm/misc

.. toctree::
   :maxdepth: 3
   :hidden:
   :caption: Units and misc. API reference

   api_ref/units
   api_ref/utils


Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
