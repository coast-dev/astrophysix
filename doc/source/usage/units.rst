.. Astrophysix documentation master file, created by sphinx-quickstart on Mon Jun  3 10:02:05 2019.
   This file is part of the 'astrophysix' Python package.
   -----------------------------------------------------------------------------------
   Copyright © Commissariat a l'Energie Atomique et aux Energies Alternatives (CEA)
   -----------------------------------------------------------------------------------
   -----------------------
   FREE SOFTWARE LICENCING
   -----------------------
   This software is governed by the CeCILL license under French law and abiding by the rules of distribution of free
   software. You can use, modify and/or redistribute the software under the terms of the CeCILL license as circulated by
   CEA, CNRS and INRIA at the following URL: "http://www.cecill.info". As a counterpart to the access to the source code
   and rights to copy, modify and redistribute granted by the license, users are provided only with a limited warranty
   and the software's author, the holder of the economic rights, and the successive licensors have only limited
   liability. In this respect, the user's attention is drawn to the risks associated with loading, using, modifying
   and/or developing or reproducing the software by the user in light of its specific status of free software, that may
   mean that it is complicated to manipulate, and that also therefore means that it is reserved for developers and
   experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the
   software's suitability as regards their requirements in conditions enabling the security of their systems and/or data
   to be ensured and, more generally, to use and operate it in the same conditions as regards security. The fact that
   you are presently reading this means that you have had knowledge of the CeCILL license and that you accept its terms.
   -----------------------
   COMMERCIAL SOFTWARE LICENCING
   -----------------------------
   You can obtain this software from CEA under other licencing terms for commercial purposes. For this you will need to
   negotiate a specific contract with a legal representative of CEA.


Physical units and constants module
===================================

The ``astrophysix`` package also provides a :class:`~astrophysix.units.unit.Unit` helper class to handle physical constants
and units. In addition, a large set of :ref:`physical quantities <phys_qties>` and :ref:`constants <phys_constants>` are defined in this module.

.. seealso::
   * The :class:`~astrophysix.units.unit.Unit` API reference.

Basic use cases
---------------

Unit information
^^^^^^^^^^^^^^^^

You can have access to unit parameters with the :attr:`~astrophysix.units.unit.Unit.name`, :attr:`~astrophysix.units.unit.Unit.description`,
:attr:`~astrophysix.units.unit.Unit.coeff`, :attr:`~astrophysix.units.unit.Unit.dimensions`,
:attr:`~astrophysix.units.unit.Unit.latex` and :attr:`~astrophysix.units.unit.Unit.physical_type` properties :

.. code-block:: python

   >>> from astrophysix import units as U
   >>> mass_unit = U.Msun
   >>> mass_unit.name
   Msun
   >>> mass_unit.dimensions
   array([1, 0, 0, 0, 0, 0, 0, 0], dtype=int32)
   >>> mass_unit.coeff
   1.9889e+30
   >>> mass_unit.description
   'Solar mass'
   >>> mass_unit.latex
   '\\textrm{M}_{\\odot}'
   >>> mass_unit.physical_type
   'mass


A summary of any :class:`~astrophysix.units.unit.Unit` can be displayed using the
:meth:`Unit.info <astrophysix.units.unit.Unit.info>` method :


.. code-block:: python

   >>> from astrophysix import units as U
   >>> U.ly.info()
   Unit : ly
   ---------
   Light year

   Value
   -----
   9460730472580800.0 m

   Equivalent units
   ----------------
    * m            :      1.057e-16 ly
    * um           :      1.057e-22 ly
    * mm           :      1.057e-19 ly
    * cm           :      1.057e-18 ly
    * nm           :      1.057e-25 ly
    * km           :      1.057e-13 ly
    * Angstrom     :      1.057e-26 ly
    * au           :    1.58125e-05 ly
    * pc           :        3.26156 ly
    * kpc          :        3261.56 ly
    * Mpc          :    3.26156e+06 ly
    * Gpc          :    3.26156e+09 ly
    * Rsun         :    7.35153e-08 ly


Unit retrieval
^^^^^^^^^^^^^^

The :ref:`built-in units and constants <phys_constants>` defined in the ``astrophysix`` package are directly accessible
as variables of the package :

.. code-block:: python

   >>> from astrophysix import units as U
   >>> U.Msun.description
   'Solar mass'
   >>> U.kHz.description
   'kilo-Hertz : frequency unit'


For custom units retrieval, see :ref:`custom_units`

.. _unit_operation:

Unit operations
^^^^^^^^^^^^^^^

You can create composite units by multiplying or dividing :class:`~astrophysix.units.unit.Unit` objects by :obj:`float`
values or other :class:`~astrophysix.units.unit.Unit` objects. You can also raise :class:`~astrophysix.units.unit.Unit`
objects to a given (integer) power :

.. code-block:: python

   >>> from astrophysix import units as U
   >>> u = U.km/U.s
   >>> print(u)
   (1000 m.s^-1)

   >>> joule = kg*(m/s)**2
   >>> joule == U.J
   True

   >>> my_p = 250.0 * J * m**-3
   >>> my_p.physical_type
   'pressure'


.. _custom_units:

Custom Units
^^^^^^^^^^^^

If you want to create your own units and use them elsewhere in your code, you can create a :class:`~astrophysix.units.unit.Unit`
instance that will be included in the ``astrophysix`` unit registry, use :meth:`Unit.create_unit <astrophysix.units.unit.Unit.create_unit>`
method to add a new unit, and :meth:`Unit.from_name <astrophysix.units.unit.Unit.from_name>` to retrieve it :

.. code-block:: python

   >>> from astrophysix import units as U
   >>> U.km_s == U.km/U.s # Soft equality => same coefficient and same dimensions
   True
   >>> U.km_s.identical(U.km/U.s)  # Strict equality => they do not share the same names/LaTEX formulae, descriptions
   False
   # Create a custom Solar mass per square kiloparsec surface density unit
   >>> u = U.Unit.create_unit(name="Msun_pc2", base_unit=U.Msun/U.pc**2, descr="Solar mass per square parsec",
                              latex="\\textrm{M}_{\\odot}\\cdot\\textrm{pc}^{-2}")

   >>> u.identical(U.Msun/U.pc**2)
   False

   >>> # Later in your Python script ...
   >>> surf_dens_unit = U.Unit.from_name("Msun_pc2")
   surf_dens_unit == U.Msun/U.pc**2
   True
   >>>surf_dens_unit.description
   "Solar mass per square parsec"


Unit search
^^^^^^^^^^^

You can browse the units available in the ``astrophysix`` unit registry using the
:meth:`Unit.equivalent_unit_list <astrophysix.units.unit.Unit.equivalent_unit_list>` method, the
:meth:`Unit.appropriate_unit <astrophysix.units.unit.Unit.appropriate_unit>` method or the
:meth:`Unit.iterate_units <astrophysix.units.unit.Unit.iterate_units>` iterator :

.. code-block:: python

   >>> # Equivalent units
   >>> U.km.equivalent_unit_list()
   [m : (1 m),
    um : (1e-06 m),
    mm : (0.001 m),
    cm : (0.01 m),
    nm : (1e-09 m),
    Angstrom : (1e-10 m),
    au : (1.49598e+11 m),
    pc : (3.08568e+16 m),
    kpc : (3.08568e+19 m),
    Mpc : (3.08568e+22 m),
    Gpc : (3.08568e+25 m),
    Rsun : (6.95508e+08 m),
    ly : (9.46073e+15 m)]

   >>> # Most appropriate unit
   >>> u = 0.3 * U.pc
   >>> f, best_unit = u.appropriate_unit()
   >>> print("{f:g} {bu:s}".format(f=f, bu=best_unit.name))
   0.978469 ly

   >>> # Unit iterator
   >>> for u in Unit.iterate_units(phys_type="time"):
           print(u.name)
   s
   min
   hour
   day
   sid_day
   year
   kyr
   Myr
   Gyr


Unit conversion
^^^^^^^^^^^^^^^

Unit conversion can be done with the :meth:`Unit.express <astrophysix.units.unit.Unit.express>` method :

.. code-block:: python

    >>> from astrophysix import units as U
    >>> # Basic unit conversion
    >>> l = 100.0 * U.kpc
    >>> t = 320.0 U. Myr
    >>> v = l/t
    >>> print("v = {c:g} km/s".format(c=v.express(U.km_s)))
    v = 305.56 km/s




Built-in quantities and constants
---------------------------------

.. automodule:: astrophysix.units