.. This file is part of the 'astrophysix' Python package documentation.
   -----------------------------------------------------------------------------------
   Copyright © Commissariat a l'Energie Atomique et aux Energies Alternatives (CEA)
   -----------------------------------------------------------------------------------
   -----------------------
   FREE SOFTWARE LICENCING
   -----------------------
   This software is governed by the CeCILL license under French law and abiding by the rules of distribution of free
   software. You can use, modify and/or redistribute the software under the terms of the CeCILL license as circulated by
   CEA, CNRS and INRIA at the following URL: "http://www.cecill.info". As a counterpart to the access to the source code
   and rights to copy, modify and redistribute granted by the license, users are provided only with a limited warranty
   and the software's author, the holder of the economic rights, and the successive licensors have only limited
   liability. In this respect, the user's attention is drawn to the risks associated with loading, using, modifying
   and/or developing or reproducing the software by the user in light of its specific status of free software, that may
   mean that it is complicated to manipulate, and that also therefore means that it is reserved for developers and
   experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the
   software's suitability as regards their requirements in conditions enabling the security of their systems and/or data
   to be ensured and, more generally, to use and operate it in the same conditions as regards security. The fact that
   you are presently reading this means that you have had knowledge of the CeCILL license and that you accept its terms.
   -----------------------
   COMMERCIAL SOFTWARE LICENCING
   -----------------------------
   You can obtain this software from CEA under other licencing terms for commercial purposes. For this you will need to
   negotiate a specific contract with a legal representative of CEA.


Installation
============

``astrophysix`` can be installed in you local Python environment (virtualenv, conda, ...) with **pip**.

Latest stable releases
----------------------

Using **pip**, you can easily download and install the latest version of the ``astrophysix`` package directly from the
`Python Package Index (PyPI)`_:

.. code-block:: bash

    > pip install astrophysix
    Collecting astrophysix
        Downloading astrophysix-0.5.0-py2.py3-none-any.whl (101 kB)
    Collecting h5py>=2.10.0
        Using cached h5py-2.10.0-cp36-cp36m-manylinux1_x86_64.whl (2.9 MB)
    Collecting Pillow>=6.2.1
        Using cached Pillow-7.2.0-cp36-cp36m-manylinux1_x86_64.whl (2.2 MB)
    Collecting numpy>=1.16.4
        Using cached numpy-1.19.2-cp36-cp36m-manylinux2010_x86_64.whl (14.5 MB)
    Collecting future>=0.17.1
        Using cached future-0.18.2.tar.gz (829 kB)
    Collecting six
        Using cached six-1.15.0-py2.py3-none-any.whl (10 kB)
    Installing collected packages: numpy, six, h5py, Pillow, future, astrophysix
        Running setup.py install for future ... done
    Successfully installed Pillow-7.2.0 astrophysix-0.5.0 future-0.18.2 h5py-2.10.0 numpy-1.19.2 six-1.15.0


.. _Python Package Index (PyPI): https://pypi.org/project/astrophysix/


Unstable releases
-----------------

If you wish to use a specific beta release of ``astrophysix``, you can select which version to install :

.. code-block:: bash

    > pip install astrophysix==0.5.0ra1
    Collecting astrophysix
        Downloading astrophysix-0.5.0a1-py2.py3-none-any.whl (101 kB)
    Collecting h5py>=2.10.0
        Using cached h5py-2.10.0-cp36-cp36m-manylinux1_x86_64.whl (2.9 MB)
    Collecting Pillow>=6.2.1
        Using cached Pillow-7.2.0-cp36-cp36m-manylinux1_x86_64.whl (2.2 MB)
    Collecting numpy>=1.16.4
        Using cached numpy-1.19.2-cp36-cp36m-manylinux2010_x86_64.whl (14.5 MB)
    Collecting future>=0.17.1
        Using cached future-0.18.2.tar.gz (829 kB)
    Collecting six
        Using cached six-1.15.0-py2.py3-none-any.whl (10 kB)
    Installing collected packages: numpy, six, h5py, Pillow, future, astrophysix
        Running setup.py install for future ... done
    Successfully installed Pillow-7.2.0 astrophysix-0.5.0a1 future-0.18.2 h5py-2.10.0 numpy-1.19.2 six-1.15.0