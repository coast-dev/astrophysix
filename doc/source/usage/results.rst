.. Astrophysix documentation master file, created by sphinx-quickstart on Mon Jun  3 10:02:05 2019.
   This file is part of the 'astrophysix' Python package.
   -----------------------------------------------------------------------------------
   Copyright © Commissariat a l'Energie Atomique et aux Energies Alternatives (CEA)
   -----------------------------------------------------------------------------------
   -----------------------
   FREE SOFTWARE LICENCING
   -----------------------
   This software is governed by the CeCILL license under French law and abiding by the rules of distribution of free
   software. You can use, modify and/or redistribute the software under the terms of the CeCILL license as circulated by
   CEA, CNRS and INRIA at the following URL: "http://www.cecill.info". As a counterpart to the access to the source code
   and rights to copy, modify and redistribute granted by the license, users are provided only with a limited warranty
   and the software's author, the holder of the economic rights, and the successive licensors have only limited
   liability. In this respect, the user's attention is drawn to the risks associated with loading, using, modifying
   and/or developing or reproducing the software by the user in light of its specific status of free software, that may
   mean that it is complicated to manipulate, and that also therefore means that it is reserved for developers and
   experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the
   software's suitability as regards their requirements in conditions enabling the security of their systems and/or data
   to be ensured and, more generally, to use and operate it in the same conditions as regards security. The fact that
   you are presently reading this means that you have had knowledge of the CeCILL license and that you accept its terms.
   -----------------------
   COMMERCIAL SOFTWARE LICENCING
   -----------------------------
   You can obtain this software from CEA under other licencing terms for commercial purposes. For this you will need to
   negotiate a specific contract with a legal representative of CEA.
.. _usage_results:

Results and associated datafiles
================================

To any `Experiment` (:class:`~astrophysix.simdm.experiment.Simulation` or :class:`~astrophysix.simdm.experiment.PostProcessingRun`),
you can attach results of your scientific analysis. There are two kinds of result available in ``astrophysix``:


+ :class:`~astrophysix.simdm.results.generic.GenericResult` : result **not strictly related to a particular instant** in
  the dynamical evolution of your numerical experiment (e.g. star formation history, solar activity cycles, planetary
  orbital decay, etc.),
+ :class:`~astrophysix.simdm.results.snapshot.Snapshot` : result **corresponding to a specific moment** during the
  numerical experiment (galactic pericentric passage, solar activity peak, star formation burst, etc.).

.. image:: ../_static/img/result_inheritance.png
   :align: center


Generic result
--------------

Here is a full example on how to create a :class:`~astrophysix.simdm.results.generic.GenericResult` object with all
optional parameters and add it into an `Experiment`, using the
:attr:`Simulation.generic_results <astrophysix.simdm.experiment.Simulation.generic_results>` or
:attr:`PostProcessingRun.generic_results <astrophysix.simdm.experiment.PostProcessingRun.generic_results>` property :

.. code-block:: python

    >>> from astrophysix.simdm.results import GenericResult
    >>>
    >>> res1 = GenericResult(name="Star formation history",
    ...                      directory_path="/my/path/to/result",
    ...                      description="""This is the star formation history during
    ...                                     the 2 Myr of the galaxy major merger""")
    >>> simu.generic_results.add(res1)



Snapshot
--------

A :class:`~astrophysix.simdm.results.snapshot.Snapshot` derives from a :class:`~astrophysix.simdm.results.generic.GenericResult`
object but with additional optional properties :

 + :attr:`~astrophysix.simdm.results.snapshot.Snapshot.time`,
 + :attr:`~astrophysix.simdm.results.snapshot.Snapshot.physical_size`,
 + :attr:`~astrophysix.simdm.results.snapshot.Snapshot.data_reference`.

Here is a full example on how to create a :class:`~astrophysix.simdm.results.snapshot.Snapshot` object and add it into
any `Experiment`, using :attr:`Simulation.snapshots <astrophysix.simdm.experiment.Simulation.snapshots>` or
:attr:`PostProcessingRun.snapshots <astrophysix.simdm.experiment.PostProcessingRun.snapshots>` property :

.. code-block:: python

   >>> from astrophysix.simdm.results import Snapshot
   >>> from astrophysix import units as U

   >>> sn34 = Snapshot(name="Third pericenter",
   ...                 time=(254.7, U.Myr),
   ...                 physical_size=(400.0, U.kpc),
   ...                 decription="""This snapshot corresponds to the third pericentric
   ...                               of the galactic major merger simulation, occurring
   ...                               around $t\simeq255 \; \\textrm{Myr}$.""",
   ...                 data_reference="34;Big_endian",
   ...                 directory_path="/my/path/to/galactic_merger/simu/outputs/output_00034")
   >>> simu.snapshots.add(sn34)


.. _data_ref_terminus:

.. note::
   The :attr:`Snapshot.data_reference <astrophysix.simdm.results.snapshot.Snapshot.data_reference>` property is only
   used by the :galactica:`Galactica web application <>` to provide a reference on your raw simulation data files to the
   on-demand post-processing services (see `Terminus documentation <https://galactica-terminus.readthedocs.io>`_.


.. _datafiles:

Datafiles
---------

One of the most important feature implemented in the ``astrophysix`` package is the possibility to insert documents into
a :class:`~astrophysix.simdm.SimulationStudy` and to describe each one of them with meta-information.

.. image:: ../_static/img/simdm_datafiles.png
   :align: center

To do so, you must create a :class:`~astrophysix.simdm.datafiles.Datafile` (the :attr:`~astrophysix.simdm.datafiles.Datafile.name`
attribute is the only one mandatory) and then add it into your :class:`~astrophysix.simdm.results.snapshot.Snapshot` (or
:class:`~astrophysix.simdm.results.generic.GenericResult`) using the
:attr:`Snapshot.datafiles <astrophysix.simdm.results.snapshot.Snapshot.datafiles>` (or
:attr:`GenericResult.datafiles <astrophysix.simdm.results.generic.GenericResult.datafiles>`) property :

.. code-block:: python

    >>> from astrophysix.simdm.datafiles import Datafile, PlotType, PlotInfo, image, file
    >>> from astrophysix.utils.file import FileType, FileUtil
    >>>
    >>> imf_df = Datafile(name="Initial mass function",
    ...                   description="This is my IMF plot detailed description...")
    >>> snapshot_100.datafiles.add(imf_df)

.. _attached_files:

Attached files
^^^^^^^^^^^^^^

Once created, a single :class:`~astrophysix.simdm.datafiles.Datafile` can contain different files, but **at most one per**
:class:`~astrophysix.utils.file.FileType`. The available :class:`~astrophysix.utils.file.FileType` values are :

 * :attr:`FileType.HDF5_FILE <astrophysix.utils.file.FileType.HDF5_FILE>`
 * :attr:`FileType.PNG_FILE <astrophysix.utils.file.FileType.PNG_FILE>`
 * :attr:`FileType.JPEG_FILE <astrophysix.utils.file.FileType.JPEG_FILE>`
 * :attr:`FileType.FITS_FILE <astrophysix.utils.file.FileType.FITS_FILE>`
 * :attr:`FileType.TARGZ_FILE <astrophysix.utils.file.FileType.TARGZ_FILE>`
 * :attr:`FileType.PICKLE_FILE <astrophysix.utils.file.FileType.PICKLE_FILE>`
 * :attr:`FileType.JSON_FILE <astrophysix.utils.file.FileType.JSON_FILE>`
 * :attr:`FileType.CSV_FILE <astrophysix.utils.file.FileType.CSV_FILE>`
 * :attr:`FileType.YAML_FILE <astrophysix.utils.file.FileType.YAML_FILE>`
 * :attr:`FileType.ASCII_FILE <astrophysix.utils.file.FileType.ASCII_FILE>`
 * :attr:`FileType.XML_FILE <astrophysix.utils.file.FileType.XML_FILE>`

To add files from your filesystem in a :class:`~astrophysix.simdm.datafiles.Datafile`, you can do it in 2 steps (create
first a :class:`~astrophysix.simdm.datafiles.file.AssociatedFile` and then put it in the :class:`~astrophysix.simdm.datafiles.Datafile`):

.. code-block:: python

    >>> import os
    >>> from astrophysix.simdm.datafiles import image, file
    >>>
    >>> # JPEG image
    >>> jpeg_filepath = os.path.join("/data", "path", "to", "my", "plots", "IMF_plot.jpg")
    >>> jpeg_image_file = image.JpegImageFile.load_file(jpeg_filepath)
    >>> imf_df[FileType.JPEG_FILE] = jpeg_image_file
    >>>
    >>> # HDF5 file
    >>> hdf5_filepath = os.path.join("/data", "path", "to", "raw", "datasets", "all_stars.h5")
    >>> hdf5_file = file.HDF5File.load_file(hdf5_filepath)
    >>> imf_df[FileType.HDF5_FILE] = hdf5_file


or if you are in a hurry, you can do it in a single one :

.. code-block:: python

    >>> import os
    >>> from astrophysix.simdm.datafiles import image, file
    >>>
    >>> imf_df[FileType.JPEG_FILE] = os.path.join("/data", "path", "plots", "IMF_plot.jpg")
    >>> imf_df[FileType.HDF5_FILE] = os.path.join("/data", "path", "datasets", "all_stars.h5")

To delete a file from a :class:`~astrophysix.simdm.datafiles.Datafile` use the ``del`` Python operator:

.. code-block:: python

    >>> del imf_df[FileType.HDF_FILE]


The :class:`~astrophysix.simdm.datafiles.file.AssociatedFile` contains your file raw byte array and has information on
the original file (filename, last modification time). It can be used to re-export your file from your
:class:`~astrophysix.simdm.SimulationStudy` to save it on your local filesystem (it even preserves the `last modification
time` of the original file):

.. code-block:: python

    >>> jpeg_image_file = imf_df[FileType.JPEG_FILE]
    >>> jpeg_image_file.last_modified
    datetime.datetime(2020, 9, 22, 10, 42, 18, tzinfo=datetime.timezone.utc)
    >>> saved_path = os.path.join("/home", "user", "Desktop", jpeg_image_file.filename)
    >>> jpeg_image_file.save_to_disk(saved_path)
    File '/home/user/Desktop/IMF_plot.jpg' saved
    >>>
    >>> import filecmp
    >>> # Is the file saved identical to the original one ?
    >>> filecmp.cmp(saved_path, jpeg_filepath, shallow=False)
    True
    >>>
    >>> from astrophysix.utils.file import FileUtil
    >>> from astrophysix.utils import DatetimeUtil
    >>> # Was the file 'last modification time' preserved ?
    >>> last_mod_tms = FileUtil.last_modification_timestamp(fpath)
    >>> last_mod_dt = DatetimeUtil.utc_from_timestamp(last_mod_tms)
    >>> last_mod_dt == jpeg_image_file.last_modified
    True

.. note::
   Since you can embed all your reduced data files into a :class:`~astrophysix.simdm.SimulationStudy`, you can safely
   remove your datafiles from your local filesystem and use the :class:`~astrophysix.simdm.SimulationStudy` HDF5 file
   as a self-contained, portable filesystem that you can exchange with your scientific collaborators.


Plot information
^^^^^^^^^^^^^^^^

A :class:`~astrophysix.simdm.datafiles.Datafile` can also have additional meta-information on a scientific plot for which
you may already have attached PNG files (or JPEG, etc.). This meta-information can be used by other users to reproduce
your plot or by the :galactica:`Galactica web application<>` to display an interactive version of your plot online.


.. code-block:: python

    >>> from astrophysix.simdm.datafiles import PlotType, PlotInfo
    >>> from astrophysix import units as U
    >>>
    >>> imf_df.plot_info = PlotInfo(plot_type=PlotType.LINE_PLOT, title="My plot title",
    ...                             xaxis_values=N.array([10.0, 20.0, , 22.0, 24.2, 30.0]),
    ...                             yaxis_values=N.array([1.2, 35.2, 5.2, 21.2, 14.9]),
    ...                             xaxis_log_scale=False, yaxis_log_scale=True,
    ...                             xlabel="x-axis label", ylabel="y-axis label",
    ...                             xaxis_unit="Myr", yaxis_unit=U.kpc)