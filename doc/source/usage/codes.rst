.. This file is part of the 'astrophysix' Python package documentation.
   -----------------------------------------------------------------------------------
   Copyright © Commissariat a l'Energie Atomique et aux Energies Alternatives (CEA)
   -----------------------------------------------------------------------------------
   -----------------------
   FREE SOFTWARE LICENCING
   -----------------------
   This software is governed by the CeCILL license under French law and abiding by the rules of distribution of free
   software. You can use, modify and/or redistribute the software under the terms of the CeCILL license as circulated by
   CEA, CNRS and INRIA at the following URL: "http://www.cecill.info". As a counterpart to the access to the source code
   and rights to copy, modify and redistribute granted by the license, users are provided only with a limited warranty
   and the software's author, the holder of the economic rights, and the successive licensors have only limited
   liability. In this respect, the user's attention is drawn to the risks associated with loading, using, modifying
   and/or developing or reproducing the software by the user in light of its specific status of free software, that may
   mean that it is complicated to manipulate, and that also therefore means that it is reserved for developers and
   experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the
   software's suitability as regards their requirements in conditions enabling the security of their systems and/or data
   to be ensured and, more generally, to use and operate it in the same conditions as regards security. The fact that
   you are presently reading this means that you have had knowledge of the CeCILL license and that you accept its terms.
   -----------------------
   COMMERCIAL SOFTWARE LICENCING
   -----------------------------
   You can obtain this software from CEA under other licencing terms for commercial purposes. For this you will need to
   negotiate a specific contract with a legal representative of CEA.

.. _usage_codes:

Protocols
=========

Numerical codes used to produce simulation data or post-process them are called `Protocols` in the
``Simulation Datamodel`` vocabulary. They can be of two types :

 * :class:`~astrophysix.simdm.protocol.SimulationCode` to run simulations,
 * :class:`~astrophysix.simdm.protocol.PostProcessingCode` to post-process simulation data.

Any `Protocol` contains a set of :class:`~astrophysix.simdm.protocol.Algorithm` and a set of
:class:`~astrophysix.simdm.protocol.InputParameter`. In addition, a :class:`~astrophysix.simdm.protocol.SimulationCode`
contains a set of :class:`~astrophysix.simdm.protocol.PhysicalProcess` :

.. image:: ../_static/img/protocol_inheritance.png
   :align: center


Simu/Post-pro codes
-------------------

To initialize a :class:`~astrophysix.simdm.protocol.SimulationCode`, you only need to set
:attr:`~astrophysix.simdm.protocol.SimulationCode.name` and :attr:`~astrophysix.simdm.protocol.SimulationCode.code_name`
properties. Here is a more complete example of a :class:`~astrophysix.simdm.protocol.SimulationCode` initialization with
all optional attributes :

.. code-block:: python

   >>> from astrophysix.simdm.protocol import SimulationCode
   >>> ramses = SimulationCode(name="Ramses 3 (MHD)", code_name="RAMSES", code_version="3.10.1",
   ...                         alias="RAMSES_3", url="https://www.ics.uzh.ch/~teyssier/ramses/RAMSES.html",
   ...                         description="This is a fair description of the Ramses code")


The same applies to the initialization of a :class:`~astrophysix.simdm.protocol.PostProcessingCode`.

.. warning::
   Setting the :attr:`SimulationCode.alias <~astrophysix.simdm.protocol.SimulationCode.alias>` and
   :attr:`PostProcessingCode.alias <~astrophysix.simdm.protocol.PostProcessingCode.alias>`
   properties is necessary only if you wish to upload your study on the :galactica:`Galactica simulation database <>`.
   See :ref:`why_an_alias` and :ref:`check_galactica_validation`



.. seealso::

   - :class:`~astrophysix.simdm.protocol.SimulationCode` API reference,
   - :class:`~astrophysix.simdm.protocol.PostProcessingCode` API reference,
   - :ref:`usage_runs` section.



Input parameters
----------------

To add :class:`~astrophysix.simdm.protocol.InputParameter` objects into any `Protocol`, use :

 * :attr:`SimulationCode.input_parameters <astrophysix.simdm.protocol.SimulationCode.input_parameters>` for a
   :class:`~astrophysix.simdm.protocol.SimulationCode`,
 * :attr:`PostProcessingCode.input_parameters <astrophysix.simdm.protocol.PostProcessingCode.input_parameters>` for a
   :class:`~astrophysix.simdm.protocol.PostProcessingCode`.


.. code-block:: python

   >>> from astrophysix.simdm.protocol import InputParameter
   >>> # One-liner
   >>> lmin = ramses.input_parameters.add(InputParameter(key="levelmin", name="Lmin",
   ...                                                   description="min. level of AMR refinement"))
   # Or
   >>> lmax = InputParameter(key="levelmax", name="Lmax", description="max. level of AMR refinement")
   >>> ramses.input_parameters.add(lmax)

To initialize an :class:`~astrophysix.simdm.protocol.InputParameter`, only the
:attr:`InputParameter.name <astrophysix.simdm.protocol.InputParameter.name>` property must be set :

.. code-block:: python

   >>> # Input parameters should be initialised with at least a 'name' attribute.
   >>> rho_min = InputParameter()
   AttributeError : Input parameter 'name' attribute is not defined (mandatory).


.. seealso::
   :class:`~astrophysix.simdm.protocol.InputParameter` API reference.


Algorithms
----------

To add :class:`~astrophysix.simdm.protocol.Algorithm` objects into any `Protocol`, use :

 * :attr:`SimulationCode.algorithms <astrophysix.simdm.protocol.SimulationCode.algorithms>` for a
   :class:`~astrophysix.simdm.protocol.SimulationCode`,
 * :attr:`PostProcessingCode.algorithms <astrophysix.simdm.protocol.PostProcessingCode.algorithms>` for a
   :class:`~astrophysix.simdm.protocol.PostProcessingCode`.


.. code-block:: python

   >>> from astrophysix.simdm.protocol import Algorithm, AlgoType
   >>> # One-liner
   >>> voronoi = arepo.algorithms.add(Algorithm(algo_type=AlgoType.VoronoiMovingMesh,
   ...                                          description="Moving mesh based on a Voronoi-Delaunay tesselation."))
   # Or
   >>> voronoi = Algorithm(algo_type=AlgoType.VoronoiMovingMesh,
   ...                     description="Moving mesh based on a Voronoi-Delaunay tesselation.")
   >>> arepo.algorithms.add(voronoi)


To initialize an :class:`~astrophysix.simdm.protocol.Algorithm`, only the
:attr:`Algorithm.algo_type <astrophysix.simdm.protocol.Algorithm.algo_type>` property must be set :

.. code-block:: python

   >>> # Algorithm should be initialised with at least an 'algo_type' attribute.
   >>> algo = Algorithm()
   AttributeError : Algorithm 'algo_type' attribute is not defined (mandatory).


Available algorithm types are :

 * :attr:`AlgoType.StructuredGrid <astrophysix.simdm.protocol.AlgoType.StructuredGrid>`
 * :attr:`AlgoType.AdaptiveMeshRefinement <astrophysix.simdm.protocol.AlgoType.AdaptiveMeshRefinement>`
 * :attr:`AlgoType.SmoothParticleHydrodynamics <astrophysix.simdm.protocol.AlgoType.SmoothParticleHydrodynamics>`
 * :attr:`AlgoType.VoronoiMovingMesh <astrophysix.simdm.protocol.AlgoType.VoronoiMovingMesh>`
 * :attr:`AlgoType.SpectralMethod <astrophysix.simdm.protocol.AlgoType.SpectralMethod>`
 * :attr:`AlgoType.Godunov <astrophysix.simdm.protocol.AlgoType.Godunov>`
 * :attr:`AlgoType.PoissonMultigrid <astrophysix.simdm.protocol.AlgoType.PoissonMultigrid>`
 * :attr:`AlgoType.PoissonConjugateGradient <astrophysix.simdm.protocol.AlgoType.PoissonConjugateGradient>`
 * :attr:`AlgoType.ParticleMesh <astrophysix.simdm.protocol.AlgoType.ParticleMesh>`
 * :attr:`AlgoType.NBody <astrophysix.simdm.protocol.AlgoType.NBody>`
 * :attr:`AlgoType.FriendOfFriend <astrophysix.simdm.protocol.AlgoType.FriendOfFriend>`
 * :attr:`AlgoType.HLLCRiemann <astrophysix.simdm.protocol.AlgoType.HLLCRiemann>`
 * :attr:`AlgoType.RayTracer <astrophysix.simdm.protocol.AlgoType.RayTracer>`
 * :attr:`AlgoType.RadiativeTransfer <astrophysix.simdm.protocol.AlgoType.RadiativeTransfer>`
 * :attr:`AlgoType.RungeKutta <astrophysix.simdm.protocol.AlgoType.RungeKutta>`

.. seealso::
   :class:`~astrophysix.simdm.protocol.Algorithm` and :class:`~astrophysix.simdm.protocol.AlgoType` API references.

Physical processes
------------------

.. note::
      For :class:`~astrophysix.simdm.protocol.SimulationCode` only.

To add :class:`~astrophysix.simdm.protocol.PhysicalProcess` objects into a
:class:`~astrophysix.simdm.protocol.SimulationCode`, use the
:attr:`SimulationCode.physical_processes <astrophysix.simdm.protocol.SimulationCode.physical_processes>` property.

.. code-block:: python

   >>> from astrophysix.simdm.protocol import PhysicalProcess, Physics
   >>> # One-liner
   >>> sf = ramses.physical_processes.add(PhysicalProcess(physics=Physics.StarFormation,
   ...                                                    description="Conversion of gas into massive star particles."))
   # Or
   >>> sf = PhysicalProcess(physics=Physics.StarFormation, description="Conversion of gas into massive star particles.")
   >>> ramses.physical_processes.add(sf)


To initialize a :class:`~astrophysix.simdm.protocol.PhysicalProcess`, only the
:attr:`PhysicalProcess.physics <astrophysix.simdm.protocol.PhysicalProcess.physics>` property must be set :

.. code-block:: python

   >>> # PhysicalProcess should be initialised with at least a 'physics' attribute.
   >>> process = PhysicalProcess()
   AttributeError : PhysicalProcess 'physics' attribute is not defined (mandatory).


Available physics are :

 * :attr:`Physics.SelfGravity <astrophysix.simdm.protocol.Physics.SelfGravity>`
 * :attr:`Physics.ExternalGravity <astrophysix.simdm.protocol.Physics.ExternalGravity>`
 * :attr:`Physics.Hydrodynamics <astrophysix.simdm.protocol.Physics.Hydrodynamics>`
 * :attr:`Physics.MHD <astrophysix.simdm.protocol.Physics.MHD>`
 * :attr:`Physics.StarFormation <astrophysix.simdm.protocol.Physics.StarFormation>`
 * :attr:`Physics.RadiativeTransfer <astrophysix.simdm.protocol.Physics.RadiativeTransfer>`
 * :attr:`Physics.AGNFeedback <astrophysix.simdm.protocol.Physics.AGNFeedback>`
 * :attr:`Physics.SupernovaeFeedback <astrophysix.simdm.protocol.Physics.SupernovaeFeedback>`
 * :attr:`Physics.SupermassiveBlackHoleFeedback <astrophysix.simdm.protocol.Physics.SupermassiveBlackHoleFeedback>`
 * :attr:`Physics.StellarIonisingRadiation <astrophysix.simdm.protocol.Physics.StellarIonisingRadiation>`
 * :attr:`Physics.StellarUltravioletRadiation <astrophysix.simdm.protocol.Physics.StellarUltravioletRadiation>`
 * :attr:`Physics.StellarInfraredRadiation <astrophysix.simdm.protocol.Physics.StellarInfraredRadiation>`
 * :attr:`Physics.ProtostellarJetFeedback <astrophysix.simdm.protocol.Physics.ProtostellarJetFeedback>`
 * :attr:`Physics.DustCooling <astrophysix.simdm.protocol.Physics.DustCooling>`
 * :attr:`Physics.MolecularCooling <astrophysix.simdm.protocol.Physics.MolecularCooling>`
 * :attr:`Physics.AtomicCooling <astrophysix.simdm.protocol.Physics.AtomicCooling>`
 * :attr:`Physics.Chemistry <astrophysix.simdm.protocol.Physics.Chemistry>`
 * :attr:`Physics.TurbulentForcing <astrophysix.simdm.protocol.Physics.TurbulentForcing>`


.. seealso::
   :class:`~astrophysix.simdm.protocol.PhysicalProcess` and :class:`~astrophysix.simdm.protocol.Physics` API references.