.. Astrophysix documentation master file, created by sphinx-quickstart on Mon Jun  3 10:02:05 2019.
   This file is part of the 'astrophysix' Python package.
   -----------------------------------------------------------------------------------
   Copyright © Commissariat a l'Energie Atomique et aux Energies Alternatives (CEA)
   -----------------------------------------------------------------------------------
   -----------------------
   FREE SOFTWARE LICENCING
   -----------------------
   This software is governed by the CeCILL license under French law and abiding by the rules of distribution of free
   software. You can use, modify and/or redistribute the software under the terms of the CeCILL license as circulated by
   CEA, CNRS and INRIA at the following URL: "http://www.cecill.info". As a counterpart to the access to the source code
   and rights to copy, modify and redistribute granted by the license, users are provided only with a limited warranty
   and the software's author, the holder of the economic rights, and the successive licensors have only limited
   liability. In this respect, the user's attention is drawn to the risks associated with loading, using, modifying
   and/or developing or reproducing the software by the user in light of its specific status of free software, that may
   mean that it is complicated to manipulate, and that also therefore means that it is reserved for developers and
   experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the
   software's suitability as regards their requirements in conditions enabling the security of their systems and/or data
   to be ensured and, more generally, to use and operate it in the same conditions as regards security. The fact that
   you are presently reading this means that you have had knowledge of the CeCILL license and that you accept its terms.
   -----------------------
   COMMERCIAL SOFTWARE LICENCING
   -----------------------------
   You can obtain this software from CEA under other licencing terms for commercial purposes. For this you will need to
   negotiate a specific contract with a legal representative of CEA.


Frequently asked questions
==========================

.. _why_an_alias:

Why an alias ?
--------------

the `alias` property in the ``astrophysix`` package is an optional parameter only mandatory within
:class:`~astrophysix.simdm.SimulationStudy` HDF5 files that are meant to be uploaded on the
:galactica:`Galactica simulation database <>`. This optional property can be found in various classes :

 * :class:`~astrophysix.simdm.Project`,
 * :class:`~astrophysix.simdm.protocol.SimulationCode`,
 * :class:`~astrophysix.simdm.protocol.PostProcessingCode`,
 * :class:`~astrophysix.simdm.experiment.Simulation`,
 * :class:`~astrophysix.simdm.experiment.PostProcessingRun`.

The `alias` must verify a specific format. For more details, see :ref:`galactica_alias_validation`.

These `alias` properties are used to reference in a unique way the projects, protocols and experiments displayed on the
web pages and appear in the URL of your web browser when you wish to visit the page of a given project or simulation. These
URLs need to be unique. For example, this is the URL of a ``ORION_FIL_MHD`` simulation of the ``ORION`` project in the
``STAR_FORM`` (:attr:`ProjectCategory.StarFormation <astrophysix.simdm.ProjectCategory.StarFormation>`) project category :

 * http://www.galactica-simulations.eu/db/STAR_FORM/ORION/ORION_FIL_MHD/


.. _check_galactica_validation:

How can I check validity for Galactica ?
----------------------------------------

When you try to upload a :class:`~astrophysix.simdm.SimulationStudy` HDF5 file onto the :galactica:`Galactica web application <>`,
the server will check the consistency of the project you are trying to upload against the content of the database. These
checks are more stringent than the ones performed by ``astrophysix`` upon saving the HDF5 file. In case your project cannot
be uploaded, a panel showing error messages will be displayed :

.. figure:: _static/img/galactica_import_error.png

   :galactica:`Galactica admin. interface <>` : an error occurred while trying to upload a
   :class:`~astrophysix.simdm.SimulationStudy` HDF5 file on the Galactica web server. The :class:`~astrophysix.simdm.experiment.Simulation`
   seems to miss its ``alias`` parameter.

This example project was created by the following script, running quite silently :

.. code-block:: python

   >>> from astrophysix.simdm import SimulationStudy, Project, ProjectCategory
   >>> from astrophysix.simdm.experiment import Simulation
   >>> from astrophysix.simdm.protocol import SimulationCode
   >>>
   >>> # Project creation + study
   >>> proj = Project(category=ProjectCategory.StarFormation, alias="ECOGAL_DEMO",
   ...                project_title="ECOGAL demo project")
   >>> study = SimulationStudy(project=proj)
   >>>
   >>> # Simulation code definition : RAMSES
   >>> ramses = SimulationCode(name="Ramses 3.0", code_name="Ramses",
   ...                         code_version="3.0.1", alias="RAMSES_3")
   >>>
   >>> # Simulation setup, with alias missing
   >>> simu = Simulation(simu_code=ramses, name="ORION MHD run",
   ...                   description="MHD simulation description",
   ...                   execution_time="2020-01-01 00:00:00")
   >>> # Add simulation into project
   >>> proj.simulations.add(simu)
   >>>
   >>> # Save study in HDF5 file
   >>> study.save_HDF5("./orion_study_light.h5")


Upon saving your study HDF5 file
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

You have the possibility to enable `Galactica` validity checks upon saving your HDF5 file by adding a ``galactica_checks=True``
option to the :func:`SimulationStudy.save_HDF5() <astrophysix.simdm.SimulationStudy.save_HDF5>` method :

.. code-block:: python

   >>> study.save_HDF5("./orion_study_light.h5", galactica_checks=True)
   [WARNING] 'ORION MHD run' simulation Galactica alias is missing.

Which tells you that your :attr:`Simulation.alias <astrophysix.simdm.experiment.Simulation.alias>` is missing.

`SimDM` object direct check
^^^^^^^^^^^^^^^^^^^^^^^^^^^

You can also directly call the :func:`~astrophysix.simdm.Project.galactica_validity_check()` method of any object of
your study to perform those checks even prior to saving your :class:`~astrophysix.simdm.SimulationStudy` HDF5 file :

.. code-block:: python

   >>> simu.galactica_validity_check()
   [WARNING] 'ORION MHD run' simulation Galactica alias is missing.

These `validity` checks include, e.g. :

   * ``alias`` format validation,
   * missing parameters in an object,
   * object name/alias unicity,
   * some ``string`` parameters maximum length,
   * minimum number of items in catalogs (:math:`n_{obj} \ge 1`).

.. _galactica_alias_validation:

Alias formatting
^^^^^^^^^^^^^^^^

Valid Galactica aliases are non-empty character ``string`` attributes of maximum length **16** which verify the
following regular expression pattern : ``^[A-Z]([A-Z0-9_]*[A-Z0-9])?$``.

.. code-block:: python

   >>> simu.alias = "_hydro_RUN_8_"
   >>> simu.galactica_validity_check()
   [WARNING] 'ORION MHD run' simulation Galactica alias is not valid (The alias can contain capital
   letters, digits and '_' only. It must start with a capital letter and cannot end with a '_'.)


.. seealso::
   FAQ section : :ref:`why_an_alias`


How to delete object from lists ?
---------------------------------

Let us assume that you wish to remove a :class:`~astrophysix.simdm.results.snapshot.Snapshot` from a
:class:`~astrophysix.simdm.experiment.Simulation`. Then you can use the standard ``del`` python operator to remove it :

.. code-block:: python

   >>> simu.snapshots
   Snapshot list :
   +---+---------------------------+--------------------------------------+
   | # |           Index           |                 Item                 |
   +---+---------------------------+--------------------------------------+
   | 0 | My best snapshot !        | 'My best snapshot !' snapshot        |
   +---+---------------------------+--------------------------------------+
   | 1 | My second best snapshot ! | 'My second best snapshot !' snapshot |
   +---+---------------------------+--------------------------------------+
   >>> del simu.snapshots[1]
   >>> simu.snapshots
   Snapshot list :
   +---+---------------------------+--------------------------------------+
   | # |           Index           |                 Item                 |
   +---+---------------------------+--------------------------------------+
   | 0 | My best snapshot !        | 'My best snapshot !' snapshot        |
   +---+---------------------------+--------------------------------------+

.. seealso::

   :class:`~astrophysix.simdm.utils.ObjectList` example in the API reference.


How to delete files from a Datafile ?
-------------------------------------

To remove a file from a :class:`~astrophysix.simdm.datafiles.Datafile`, you can use the standard ``del`` python operator:

.. code-block:: python

   >>> from astrophysix.utils.file import FileType
   >>>
   >>> power_spectrum_datafile.display_files()
   [Power scpectrum] datafile. Attached files :
   +-----------+-----------------------------+
   | File type |          Filename           |
   +-----------+-----------------------------+
   | PNG       | spectrum_1.png              |
   +-----------+-----------------------------+
   | JPEG      | spectrum_with_overlays.jpg  |
   +-----------+-----------------------------+
   >>>
   >>> del power_spectrum_datafile[FileType.PNG_FILE]
   >>> power_spectrum_datafile.display_files()
   [Power scpectrum] datafile. Attached files :
   +-----------+-----------------------------+
   | File type |          Filename           |
   +-----------+-----------------------------+
   | JPEG      | spectrum_with_overlays.jpg  |
   +-----------+-----------------------------+

.. seealso::

   + :class:`~astrophysix.simdm.datafiles.Datafile` example in the API reference,
   + :ref:`attached_files` detailed section.


.. _catalog_size_modification:

How to add/remove objects in an existing Catalog ?
---------------------------------------------------

To modify the size of an existing :class:`~astrophysix.simdm.catalogs.catalog.Catalog`, you **must first clear it
completely** and then reinsert new :class:`~astrophysix.simdm.catalogs.field.CatalogField` instances :

.. code-block::

   >>> # ----- Catalog contained 200 objects, now we want 354 ! ----- #
   >>> # Create list of new catalog fields (same as old fields, but with more objects
   >>> new_fields = [CatalogFields(obj_prop=f.object_property, values=N.random.uniform(size=354))
   ...               for f in wrong_size_cat.catalog_fields]
   >>> # Empty the catalog fields
   >>> wrong_size_cat.catalog_fields.clear()
   >>> # Insert the new fields (size: 354 objects)
   >>> for new_field in new_fields:
   ...     wrong_size_cat.catalog_fields.add(new_field)