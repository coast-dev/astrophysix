#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import numpy as N
import json

from astrophysix.simdm import SimulationStudy, Project, ProjectCategory
from astrophysix.simdm.catalogs import TargetObject, ObjectProperty, PropertySortFlag, PropertyFilterFlag, \
    ObjectPropertyGroup, Catalog, CatalogField
from astrophysix.simdm.experiment import Simulation, AppliedAlgorithm, ParameterSetting, ParameterVisibility,\
    ResolvedPhysicalProcess
from astrophysix.simdm.protocol import SimulationCode, AlgoType, Algorithm, InputParameter, PhysicalProcess, Physics
from astrophysix.simdm.results import GenericResult, Snapshot
from astrophysix.simdm.datafiles import Datafile, PlotType, PlotInfo, AsciiFile
from astrophysix.simdm.services import DataProcessingService, CatalogDataProcessingService, CatalogFieldBinding
from astrophysix.simdm.utils import DataType
from astrophysix.utils.file import FileType
from astrophysix import units as U


EH_descr = """<p>The Extreme Horizon cosmological simulation of our universe is a Grand Challenge project hosted at <a href="http://www-hpc.cea.fr/en/complexe/tgcc.htm" target="_blank">TGCC</a> supercomputing center (CEA) in 2020
and used ~50 million hours of CPU time on the <a href="https://www.top500.org/system/179700/" target="_blank">Joliot Curie</a> supercomputer.</p>

<div style="text-align:center"><a href="http://www.galactica-simulations.eu/EH_L50/index.html" target="_blank">
    <img alt="Extreme Horizon simulation interactive map" src="/media/attached_images/projects/MzItcHJvamV4dHJlbWVfaG9yaXpvbnMtY2F0Y29zbW9sb2d5/eh_l50_tilemap.jpeg" style="display:block; width:100%" />
    Interacive map of a 50x50x5 Mpc projected data of the Extreme Horizon simulation at $Z\simeq 2$. The available data channels for visualization are the gas density, entropy, metallicity and velocity
    with an additional overlay of gas velocity streamlines.</a>
</div>

<h4><a href="#intro" rel="text-decoration:none;">Introduction</a></h4>

<p>Early-type galaxies (ETGs) at redshifts of $z >1.5$ are much more compact than nearby ones (<a target="_blank" href="https://ui.adsabs.harvard.edu/link_gateway/2005ApJ...626..680D/doi:10.1086/430104">Daddi et al. 2005</a>).
At stellar masses about $10^{11} \; \Msun$, they typically have half-mass radii of 0.7-3 kpc, which is about three times smaller than nearby ellipticals with similar masses
(<a  target="_blank" href="https://ui.adsabs.harvard.edu/link_gateway/2014ApJ...788...28V/doi:10.1088/0004-637X/788/1/28">van der Wel et al. 2014</a>). Compact radii are typically accompanied by steep luminosity profiles and high Sersic indices
(<a target="_blank" href="https://ui.adsabs.harvard.edu/link_gateway/2010ApJ...718L..73V/doi:10.1088/2041-8205/718/2/L73">van Dokkum &amp; Brammer 2010</a>;
<a target="_blank" href="https://ui.adsabs.harvard.edu/link_gateway/2013ApJ...773..112C/doi:10.1088/0004-637X/773/2/112">Carollo et al. 2013</a>).
Star-forming galaxies (SFGs) also decrease in size with increasing redshift (e.g., <a target="_blank" href="https://ui.adsabs.harvard.edu/link_gateway/2009ApJ...705L..71K/doi:10.1088/0004-637X/705/1/L71">Kriek et al. 2009</a>;
<a target="_blank" href="https://doi.org/10.1111/j.1365-2966.2010.17555.x">Dutton et al. 2011</a>). In addition, the CANDELS survey has discovered a population of very compact SFGs at $z\simeq 2$, that is,
so-called "blue nuggets" (<a target="_blank" href="https://ui.adsabs.harvard.edu/link_gateway/2013ApJ...765..104B/doi:10.1088/0004-637X/765/2/104">Barro et al. 2013</a>; 
<a target="_blank" href="https://ui.adsabs.harvard.edu/link_gateway/2014ApJ...780....1W/doi:10.1088/0004-637X/780/1/1">Williams et al. 2014</a>), which have stellar masses of $10^{10-11} \; \Msun$ and unusually small
effective radii around 2 kpc and sometimes even below 1 kpc. Compact SFGs have high comoving densities, of about $10^{-4} \; \\textrm{Mpc}^{-3}$ for stellar masses above 
$10^{10} \; \Msun$ and $10^{-5} \; \\textrm{Mpc}^{-3}$ above $10^{11} \; \Msun$ (<a target="_blank" href="https://doi.org/10.1038/s41586-019-1452-4">Wang et al. 2019</a>).
Also, SFGs at $z\simeq 2$ often have very compact gas and star formation distributions (<a target="_blank" href="https://ui.adsabs.harvard.edu/link_gateway/2018A&A...616A.110E/doi:10.1051/0004-6361/201732370">Elbaz et al. 2018</a>).</p>

<p>A number of processes have been proposed to explain the formation of compact galaxies, ranging from early formation in a compact Universe
(<a target="_blank" href="https://ui.adsabs.harvard.edu/link_gateway/2016ApJ...833....1L/doi:10.3847/0004-637X/833/1/1">Lilly &amp; Carollo 2016</a>) and the compaction of initially extended galaxies
(<a target="_blank" href="https://doi.org/10.1093/mnras/stv740">Zolotov et al. 2015</a>) and up through processes that may include galaxy mergers, disk instabilities 
(<a target="_blank" href="https://doi.org/10.1086/522077">Bournaud et al. 2007a</a>; <a target="_blank" href="https://doi.org/10.1093/mnras/stt2331">Dekel &amp; Burkert 2014</a>),
triaxial haloes (<a target="_blank" href="https://doi.org/10.1093/mnras/stw606">Tomassetti et al. 2016</a>), accretion of counter-rotating gas (<a target="_blank" href="https://doi.org/10.1093/mnras/stv270">Danovich et al. 2015</a>),
and gas return from a low-angular momentum fountain (<a target="_blank" href="https://doi.org/10.1088/0004-637X/796/2/110">Elmegreen et al. 2014</a>).</p>

<p>The <a href="EH_L50/">Extreme-Horizon (EH) cosmological simulation</a> models galaxy-formation processes with the same approach as Horizon-AGN (<a target="_blank" href="https://doi.org/10.1093/mnras/stu1227">Dubois et al. 2014</a>)
and with a substantially increased resolution in the intergalactic and circumgalactic medium (IGM and CGM).
The properties of massive galaxies in the Extreme Horizon simulation and the origin of their compactness are discused <a href="EH_L50/">here</a>. The results of this
simulation on the formation of compact galaxies are published in <a href="https://ui.adsabs.harvard.edu/link_gateway/2020A&amp;A...643L...8C/doi:10.1051/0004-6361/202038614" target="_blank">Chabanier 2020</a>.</p>

<p><button type="button" class="btn btn-primary" onClick="location.href='EH_L50/'"><span class="glyphicon glyphicon-chevron-right"></span> See simulation details ...</button></p>

<h4><a href="#acknowledgements" rel="text-decoration:none;">Acknowledgements</a></h4>
<p>This work was granted access to the HPC resources of TGCC under the allocation 2019-A0070402192 made by GENCI and a <strong>Grand Challenge</strong> project granted by GENCI on the AMD-Rome extension of the Joliot-Curie
supercomputer at TGCC. This research is part Horizon-UK projects.</p>
<div><img src="/media/attached_images/projects/MzItcHJvamV4dHJlbWVfaG9yaXpvbnMtY2F0Y29zbW9sb2d5/logosehjune2020.png" style="display:block; width:100%" /></div>
"""
# ----------------------------------------------- Project creation --------------------------------------------------- #
# Available project categories are :
proj = Project(category=ProjectCategory.Cosmology, project_title="Extreme Horizon", alias="EXTREME_HORIZONS",  # Do not change the alias  ! Otherwise another project will be created on Galactica.
               short_description="Resolving galactic disks in their cosmic environment",
               general_description=EH_descr, data_description="""<p>The following data is available from this project :</p>
               <ul>
                 <li>simulation snapshot at $Z\simeq 3$</li>
                 <li>simulation snapshot at $Z\simeq 2$</li>
                 <li>simulation snapshot at $Z\simeq 1$</li>
               </ul>""",
               acknowledgement="""<p>To acknowledge this project, please cite :</p>
               <div><pre>
               @ARTICLE{2020A&amp;A...643L...8C,
                   author = {{Chabanier}, S. and {Bournaud}, F. and {Dubois}, Y. and {Codis}, S. and {Chapon}, D. and {Elbaz}, D. and {Pichon}, C. and {Bressand}, O. and {Devriendt}, J. and {Gavazzi}, R. and {Kraljic}, K. and {Kimm}, T. and {Laigle}, C. and {Lekien}, J. -B. and {Martin}, G. and {Palanque-Delabrouille}, N. and {Peirani}, S. and {Piserchia}, P. -F. and {Slyz}, A. and {Trebitsch}, M. and {Y{\`e}che}, C.},
                   title = &quot;{Formation of compact galaxies in the Extreme-Horizon simulation}&quot;,
                   journal = {\aap},
                   keywords = {galaxies: formation, galaxies: evolution, galaxies: high-redshift, galaxies: structure, methods: numerical, Astrophysics - Astrophysics of Galaxies},
                   year = 2020,
                   month = nov,
                   volume = {643},
                   eid = {L8},
                   pages = {L8},
                   doi = {10.1051/0004-6361/202038614},
                   archivePrefix = {arXiv},
                   eprint = {2007.04624},
                   primaryClass = {astro-ph.GA},
                   adsurl = {https://ui.adsabs.harvard.edu/abs/2020A&amp;A...643L...8C},
                   adsnote = {Provided by the SAO/NASA Astrophysics Data System}
               }</pre></div>""",
               thumbnail_image="./EH_L50_cropped.jpg")
# -------------------------------------------------------------------------------------------------------------------- #


# --------------------------------------- Simulation code definition ------------------------------------------------- #
ramses = SimulationCode(name="Ramses 3", code_name="Ramses", code_version="3.0", alias="RAMSES3",
                        url="https://www.ics.uzh.ch/~teyssier/ramses/RAMSES.html",
                        description="")
# => Add algorithms
amr = ramses.algorithms.add(Algorithm(algo_type=AlgoType.AdaptiveMeshRefinement, description="AMR descr"))
ramses.algorithms.add(Algorithm(algo_type=AlgoType.Godunov, description="Godunov scheme"))
ramses.algorithms.add(Algorithm(algo_type=AlgoType.HLLCRiemann, description="HLLC Riemann solver"))
ramses.algorithms.add(Algorithm(algo_type=AlgoType.PoissonMultigrid, description="Multigrid Poisson solver"))
ramses.algorithms.add(Algorithm(algo_type=AlgoType.ParticleMesh, description="PM solver"))

# => Add input parameters
omega_m = ramses.input_parameters.add(InputParameter(key="omega_m", name="$\Omega_{m}$",
                                                     description="$\Lamda\\textrm{CDM}$ cosmology density matter"))
omega_lambda = ramses.input_parameters.add(InputParameter(key="omega_l", name="$\Omega_{\Lambda}$",
                                                          description="$\Lamda\\textrm{CDM}$ cosmology dark energy matter"))
omega_b = ramses.input_parameters.add(InputParameter(key="omega_b", name="$\Omega_{b}$",
                                                     description="$\Lamda\\textrm{CDM}$ cosmology baryon density matter"))
sigma_8 = ramses.input_parameters.add(InputParameter(key="sigma_8", name="$\sigma_{8}$",
                                                     description="$\Lamda\\textrm{CDM}$ cosmology matter power spectrum amplitude"))
H_0 = ramses.input_parameters.add(InputParameter(key="H_0", name="$\\textrm{H}_{0}$", description="Hubble constant"))
n_s = ramses.input_parameters.add(InputParameter(key="n_s", name="$\\textrm{n}_{s}$",
                                                 description="$\Lamda\\textrm{CDM}$ cosmology scalar spectral index"))

boxlen = ramses.input_parameters.add(InputParameter(key="boxlen", name="box size",
                                                    description="physical size of the simulation box"))
lmin = ramses.input_parameters.add(InputParameter(key="levelmin", name="Lmin",
                                                  description="min. level of AMR refinement"))
lmax = ramses.input_parameters.add(InputParameter(key="levelmax", name="Lmax",
                                                  description="max. level of AMR refinement"))
eps_sf = ramses.input_parameters.add(InputParameter(key="eff", name="$\epsilon_{*}$",
                                                    description="star formation efficiency"))
rho0 = ramses.input_parameters.add(InputParameter(key="rho0", name="$\\rho_{0}$",
                                                  description="star formation density threshold"))

# => Add physical processes : available physics are :
# - Physics.SelfGravity
# - Physics.ExternalGravity
# - Physics.Hydrodynamics
# - Physics.MHD
# - Physics.StarFormation
# - Physics.SupernovaeFeedback
# - Physics.AGNFeedback
# - Physics.SupermassiveBlackHoleFeedback
# - Physics.StellarIonisingRadiation
# - Physics.StellarUltravioletRadiation
# - Physics.StellarInfraredRadiation
# - Physics.ProtostellarJetFeedback
# - Physics.Chemistry
# - Physics.AtomicCooling
# - Physics.DustCooling
# - Physics.MolecularCooling
# - Physics.TurbulentForcing
# - Physics.RadiativeTransfer

ramses.physical_processes.add(PhysicalProcess(physics=Physics.StarFormation, description="Stear particle formation"))
ramses.physical_processes.add(PhysicalProcess(physics=Physics.Hydrodynamics, description="Hydrodynamics"))
grav = ramses.physical_processes.add(PhysicalProcess(physics=Physics.SelfGravity, description="Self-gravity"))
ramses.physical_processes.add(PhysicalProcess(physics=Physics.SupernovaeFeedback, description="Supernovae feedback"))
# -------------------------------------------------------------------------------------------------------------------- #


# -------------------------------------------- Simulation setup ------------------------------------------------------ #
eh_simu_descr = """The Extreme Horizon simulation (EH) is performed with the adaptive mesh refinement code RAMSES (<a target="_blank" href="https://doi.org/10.1051/0004-6361:20011817">Teyssier 2002</a>)
using the physical models from Horizon-AGN (<a target="_blank" href="https://doi.org/10.1093/mnras/stu1227">Dubois et al. 2014</a>). The spatial resolution in the CGM and IGM is largely increased compared
to Horizon-AGN, while the resolution inside galaxies is identical, at the expense of a smaller box size of $50 \; \\textrm{Mpc}\cdot\\textrm{h}^{-1}$. The control simulation of the same box with a resolution
similar to Horizon-AGN is called Standard-Horizon (SH). EH and SH share initial conditions realized with <i>mpgrafic</i> (<a target="_blank" href="https://doi.org/10.1086/590370">Prunet et al. 2008</a>).
These use a $\Lambda\\textrm{CDM}$ cosmology with matter density $\Omega_{\\textrm{m}} = 0.272$, dark energy density $\Omega_{\Lambda} = 0.728$, matter power spectrum amplitude $\sigma_{8} = 0.81$,
baryon density $\Omega_{\\textrm{b}} = 0.0455$, Hubble constant $\\textrm{H}_{0} = 70.4 \; \\textrm{km}\cdot\\textrm{s}^{-1}\cdot\\textrm{Mpc}^{-1}$, and scalar spectral index $\\textrm{n}_{\\textrm{s}} = 0.967$,
based on the WMAP-7 cosmology (<a target="_blank" href="https://doi.org/10.1088/0067-0049/192/2/18">Komatsu et al. 2011</a>). The Extreme Horizon simulation was performed on 25 000 cores of the AMD-Rome
partition of the <a href="https://www.top500.org/system/179700/" target="_blank">Joliot Curie supercomputer</a> at TGCC and it partly used the <tt>Hercule</tt> parallel I/O library
(<a target="_blank" href="http://www-physique-chimie.cea.fr/science-en-ligne/docs/chocs/Chocs_41.pdf">Bressand et al. 2012</a>; <a target="_blank" href="https://doi.org/10.1088/1742-6596/1623/1/012019">Strafella &amp; Chapon 2020</a>).
It is being run down to $z\simeq 1.0$.

<h4><a href="#strategy" rel="text-decoration:none;">Resolution strategy</a></h4>

The Standard-Horizon simulation uses a $512^{3}$ coarse grid, with a minimal resolution of $100 \; \\textrm{kpc}\cdot\\textrm{h}^{-1}$ as in Horizon-AGN.
Cells are refined up to a resolution of $\simeq 1\; \\textrm{kpc}$ in a quasi-Lagrangian manner: any cell is refined if
$\\rho_{\\textrm{DM}} \Delta\\textrm{x}^{3} + (\Omega_{\\textrm{b}}/\Omega_{\\textrm{DM}})\\rho_{\\textrm{baryon}}\Delta x^{3} > \\textrm{m}_{\\textrm{refine},\\textrm{SH}}\\textrm{M}_{\\textrm{DM},\\textrm{res}}$,
where $\\rho_{\\textrm{DM}}$ and $\\rho_{\\textrm{baryons}}$ are dark matter (DM) and baryon densities respectively in the cell, $\Delta x^{3}$ is the cell volume,
and $\\textrm{m}_{\\textrm{refine},\\textrm{SH}}=80$. The $\Omega_{\\textrm{b}}/\Omega_{\\textrm{DM}}$ factor ensures that baryons dominate the refinement condition as
soon as there is a baryon overdensity. This resolution strategy matches that of Horizon-AGN :

<table width="100%">
  <caption style="caption-side:bottom;">The first two lines indicate the comoving and physical (at $z=2$) grid resolution in $\\textrm{kpc}\cdot\\textrm{h}^{-1}$ and $\\textrm{kpc}$, respectively.
The last three lines indicate the volume fractions measured at each resolution level at $z=2$ in EH, SH and HAGN for comparison.
In the last two columns, $z<2$ means that these levels are not triggered yet at $z=2$ but will be for lower redshifts.</caption><tbody>
  <tr>
    <td align="center">Comoving grid resolution $[\\textrm{kpc}\cdot\\textrm{h}^{-1}]$</td>
    <td align="center">97.6</td>
    <td align="center">48.8</td>
    <td align="center">24.4</td>
    <td align="center">12.2</td>
    <td align="center">6.1</td>
    <td align="center">3.05</td>
    <td align="center">1.52</td>
    <td align="center">0.76</td>
  </tr>
  <tr>
    <td align="center">Physical grid resolution [kpc] ($z=2$)</td>
    <td align="center">47</td>
    <td align="center">23.5</td>
    <td align="center">11.7</td>
    <td align="center">5.8</td>
    <td align="center">2.9</td>
    <td align="center">1.5</td>
    <td align="center">0.7</td>
    <td align="center">0.3</td>
  </tr>
  <tr>
    <td colspan="9"><hr></td>
  </tr>
  <tr>
    <td align="center">Volume fraction (EH) ($z=2$)</td>
    <td align="center">-</td>
    <td align="center">45%</td>
    <td align="center">43%</td>
    <td align="center">10%</td>
    <td align="center">1%</td>
    <td align="center">0.04%</td>
    <td align="center">$z<2$</td>
    <td align="center">$z<2$</td>
  </tr>
  <tr>
    <td align="center">Volume fraction (SH) ($z=2$)</td>
    <td align="center">80%</td>
    <td align="center">17%</td>
    <td align="center">2%</td>
    <td align="center">0.17 %</td>
    <td align="center">0.013%</td>
    <td align="center">$5\\times10^{-4} %$</td>
    <td align="center">$z<2$</td>
    <td align="center">$z<2$</td>
  </tr>
  <tr>
    <td align="center">Volume fraction (HAGN) ($z=2$)</td>
    <td align="center">77%</td>
    <td align="center">19%</td>
    <td align="center">2%</td>
    <td align="center">0.2 %</td>
    <td align="center">0.01%</td>
    <td align="center">$6\\times10^{-4} %$</td>
    <td align="center">$z<2$</td>
    <td align="center">$z<2$</td>
  </tr>
</tbody></table></div>

<p>The Extreme Horizon simulation uses a $1024^{3}$ coarse grid and a more aggressive refinement strategy with $\\textrm{m}_{\\textrm{refine},\\textrm{EH}}=1/40 \, \\textrm{m}_{\\textrm{refine},\\textrm{SH}}$
in the IGM/CGM (for $\Delta x > 1.52 \; \\textrm{kpc}\cdot\\textrm{h}^{-1}$), but with $\\textrm{m}_{\\textrm{refine},\\textrm{EH}}=\\textrm{m}_{\\textrm{refine},\\textrm{SH}}$
near to and in galaxies: the whole volume is resolved with a resolution that is twice as high and most of the mass is resolved with a four times higher resolution in 1D,
yielding an improvement of 8 to 64 for the 3-D resolution. This improvement continues until the highest resolution of $\simeq1 \; \\textrm{kpc}$ is reached.
Such an aggressive approach for grid refinement can better model the early collapse of structures (<a target="_blank" href="https://doi.org/10.1086/432645">O'Shea et al. 2005</a>).
Appendix A illustrates the resolution achieved in representative regions of the CGM and IGM in EH and SH. The resolution in EH haloes is typically $\sim 6\; \\textrm{kpc}$,
while it is $\sim 25\;\\textrm{kpc}$ for SH. However, galaxies themselves are treated at the very same resolution in EH and SH: any gas denser than
$0.1 \; \\textrm{cm}^{-3}$ is resolved at the highest level in SH, as is also the case for 90% of the stellar mass.</p>

<h4><a href="#baryonic_physics" rel="text-decoration:none;">Baryonic physics</a></h4>

<p>Like in Horizon-AGN (<a target="_blank" href="https://doi.org/10.1093/mnras/stu1227">Dubois et al. 2014</a>), reionization takes place after a redshift of 10 due to
heating from a uniform UV background from <a target="_blank" href="https://doi.org/10.1086/177035">Haardt &amp; Madau (1996)</a>. There is H and He cooling implemented as
well as metal cooling, following the <a target="_blank" href="https://doi.org/10.1086/191823">Sutherland &amp; Dopita (1993)</a> model.</p>
<p>Star formation occurs in cells with a hydrogen number density larger than $\\rho_{0}=0.1 \; \\textrm{H}/\\textrm{cm}^{3}$. The star formation rate density is
$\dot{\\rho_{*}} = \epsilon_{*}\\textrm{t}_{\\textrm{ff}}$, where $\\textrm{t}_{\\textrm{ff}}$ is the local gas free-fall time and $\epsilon_{*}=0.02$ is the star-formation
efficiency (<a target="_blank" href="https://doi.org/10.1086/305588">Kennicutt 1998</a>). Mass, energy, and metals are released by stellar winds, with type Ia and type II
supernovae, assuming a Salpeter Initial Mass Function.</p>
<p>Black holes (BH) are represented by sink particles with an initial mass of $10^{5} \; \Msun$. They accrete gas through an Eddington-limited Bondi-Hoyle-Lyttleton model.
Boosted accretion episodes are included when the gas density overcomes a density threshold aimed at mitigating resolution effects, with the boosting calibrated to produce
realistic BH masses. The AGN feedback comes in two modes (<a href="https://doi.org/10.1111/j.1365-2966.2011.20236.x">Dubois et al. 2012</a>): the quasar mode injects thermal
energy and the radio mode injects mass, momentum, and kinetic energy into the surrounding medium. For a detailed parameterization of these models, we refer to 
<a target="_blank" href="https://doi.org/10.1093/mnras/stu1227">Dubois et al. (2014)</a>, the analysis of <a target="_blank" href="https://doi.org/10.1093/mnras/stw2265">Dubois et al. (2016)</a>,
and <a target="_blank" href="https://doi.org/10.1111/j.1365-2966.2011.20236.x">Dubois et al. (2012)</a>.</p>
"""
simu = Simulation(simu_code=ramses, name="Extreme Horizon (50 Mpc run)", alias="EH_L50",  # Do not change the alias  ! Otherwise another simulation will be created on Galactica.
                  description=eh_simu_descr,
                  execution_time="2020-03-01 18:45:30", directory_path="/path/to/my/project/simulation/data/",
                  # config_file=AsciiFile.load_file(os.path.join("/run", "cfg", "EH_L50", "50Mpc_run.nml"))
                  )
# Insert simulation into project
proj.simulations.add(simu)

# Add applied algorithms implementation details. Warning : corresponding algorithms must have been added in the 'ramses'
# simulation code.
simu.applied_algorithms.add(AppliedAlgorithm(algorithm=amr, details="RAMSES octree AMR implementation [Teyssier 2002]"))
simu.applied_algorithms.add(AppliedAlgorithm(algorithm=ramses.algorithms[AlgoType.HLLCRiemann.name],
                                             details="Riemann solver implementation [Teyssier 2002]"))

# Add parameter setting. Warning : corresponding input parameter must have been added in the 'ramses' simulation code.
# Available parameter visibility options are :
# - ParameterVisibility.NOT_DISPLAYED
# - ParameterVisibility.ADVANCED_DISPLAY
# - ParameterVisibility.BASIC_DISPLAY

# Cosmo parameters
# Create a user-custom unit to define the universe expansion speed
km_s_Mpc = U.Unit.create_unit(name="km_s_Mpc_", base_unit=U.km/U.s/U.Mpc, descr="universe expansion speed", latex="$\\textrm{km}\cdot\\textrm{s}^{-1}\cdot\\textrm{Mpc}^{-1}$")
simu.parameter_settings.add(ParameterSetting(input_param=H_0, value=70.4, unit=km_s_Mpc, visibility=ParameterVisibility.BASIC_DISPLAY))
simu.parameter_settings.add(ParameterSetting(input_param=omega_m, value=0.272, visibility=ParameterVisibility.BASIC_DISPLAY))
simu.parameter_settings.add(ParameterSetting(input_param=omega_lambda, value=0.728, visibility=ParameterVisibility.BASIC_DISPLAY))
simu.parameter_settings.add(ParameterSetting(input_param=omega_b, value=0.04455, visibility=ParameterVisibility.BASIC_DISPLAY))
simu.parameter_settings.add(ParameterSetting(input_param=sigma_8, value=0.81, visibility=ParameterVisibility.BASIC_DISPLAY))
simu.parameter_settings.add(ParameterSetting(input_param=n_s, value=0.967, visibility=ParameterVisibility.BASIC_DISPLAY))


# Create a user-custom unit to define the simulation domain size in comobile length unit
comMpc = U.Unit.create_unit(name="Mpc_h-1", base_unit=U.Mpc/0.704, descr="comobile Mpc", latex="$\\textrm{Mpc} \cdot \\textrm{h}^{-1}$")
comkpc = U.Unit.create_unit(name="kpc_h-1", base_unit=U.kpc/0.704, descr="comobile kpc", latex="$\\textrm{kpc} \cdot \\textrm{h}^{-1}$")
boxlen_50Mpc = simu.parameter_settings.add(ParameterSetting(input_param=boxlen, value=50, unit=comMpc,
                                                            visibility=ParameterVisibility.BASIC_DISPLAY))
simu.parameter_settings.add(ParameterSetting(input_param=lmin, value=10,
                                             visibility=ParameterVisibility.BASIC_DISPLAY))
simu.parameter_settings.add(ParameterSetting(input_param=lmax, value=16,
                                             visibility=ParameterVisibility.BASIC_DISPLAY))
simu.parameter_settings.add(ParameterSetting(input_param=eps_sf, value=2.0, unit=U.percent,
                                             visibility=ParameterVisibility.BASIC_DISPLAY))
simu.parameter_settings.add(ParameterSetting(input_param=rho0, value=0.1, unit=U.H_cc,
                                             visibility=ParameterVisibility.BASIC_DISPLAY))

# Add resolved physical process implementation details. Warning : corresponding physical process must have been added to
# the 'ramses' simulation code
simu.resolved_physics.add(ResolvedPhysicalProcess(physics=ramses.physical_processes[Physics.StarFormation.name],
                                                  details="Star formation specific implementation [Teyssier 2002]"))
simu.resolved_physics.add(ResolvedPhysicalProcess(physics=ramses.physical_processes[Physics.SelfGravity.name],
                                                  details="Self-gravity implementation details ([Teyssier 2002], [Guillet and Teyssier 2011])"))
# -------------------------------------------------------------------------------------------------------------------- #


# -------------------------------------- Simulation generic result and snapshots ------------------------------------- #
# Generic result
stellar_mass_distrub_descr = """There are 37 698 galaxies detected in Extreme Horizon at $z\sim2$ and 20 314 in Standard Horizon,
with stellar mass functions attached below.
While the mass functions above $10^{10} \; \Msun$ are quite similar in both simulations, Extreme Horizon forms twice as
many galaxies as Standard Horison with stellar mass $\\textrm{M}_{*} \le 5\\times 10^{9} \; \Msun$. We rule out any detection
bias since stellar particles have similar masses in both simulations (new stars form at the maximal resolution level in each
simulation) and we attribute this difference to the increased resolution in low-density regions.
Fitting the $z=2$ mass function with a power law of the form $\Phi(\\textrm{M}_{*}) \propto \\textrm{M}_{*}^{\\beta}$ in
the $10^{9} \le \log(\\textrm{M}_{*}/\Msun) \le 10^{9.5}$ range yields $\\beta \simeq -0.68$ for Extreme Horizon and $-0.34$ for Standard horizon.
Observations indicate a slope of $-1.0 \le \\beta \le -0.5$ in this mass range 
(<a href="https://doi.org/10.1051/0004-6361/201117513" target="_blank"> Santini et al. 2012</a>; 
<a href="https://doi.org/10.1088/0004-637X/783/2/85" target="_blank">Tomczak et al. 2014</a>),
demonstrating that low-mass galaxy formation is substantially under-resolved or delayed in Standard Horizon."""
stell_mass_distrib = GenericResult(name="Stellar mass distribution", description=stellar_mass_distrub_descr,
                                   directory_path="/my/path/to/result")
simu.generic_results.add(stell_mass_distrib)
smd_df = Datafile(name="Stellar mass functions", description="""Number of galaxies per mass bin in Extreme Horizon and Standard Horizon at $z = 2$, $3$, and $4$.""")
smd_df[FileType.PNG_FILE] = "./stellar_mass_functions.png"
stell_mass_distrib.datafiles.add(smd_df)



# Simulation snapshot
# In one-line
# sn = simu.snapshots.add(Snapshot(name="My best snapshot !", description="My first snapshot description",
#                                  time=(125, U.kyr), physical_size=(250.0, U.kpc), directory_path="/path/to/snapshot1",
#                                  data_reference="output_00056"))
# Or create snapshot, then add it to the simulation

sn_z4 = Snapshot(name="$Z \sim 4$", description="Simulation snapshot at redshift $Z \simeq 4$", time=1.0/(4.0+1),
                 physical_size=(boxlen_50Mpc.value, U.Mpc), directory_path="/path/to/EH_L50/outputs/output_00065",
                 data_reference="output_00065")
simu.snapshots.add(sn_z4)
sn_z3 = Snapshot(name="$Z \sim 3$", description="Simulation snapshot at redshift $Z \simeq 3$", time=1.0/(3.0+1),
                 physical_size=(boxlen_50Mpc.value, U.Mpc), directory_path="/path/to/EH_L50/outputs/output_00077",
                 data_reference="output_00077")
simu.snapshots.add(sn_z3)
sn_z2 = Snapshot(name="$Z \sim 2$", description="Simulation snapshot at redshift $Z \simeq 2$", time=1.0/(2.0+1),
                 physical_size=(boxlen_50Mpc.value, U.Mpc), directory_path="/path/to/EH_L50/outputs/output_00149",
                 data_reference="output_00149")
simu.snapshots.add(sn_z2)
# -------------------------------------------------------------------------------------------------------------------- #


# ---------------------------------------------------- Result datafiles ---------------------------------------------- #
# Datafile creation
# imf_df = sn.datafiles.add(Datafile(name="Initial mass function plot",
#                                    description="This is my plot detailed description"))

# Add attached files to a datafile (1 per file type). Available file types are :
# - FileType.HDF5_FILE
# - FileType.PNG_FILE
# - FileType.JPEG_FILE
# - FileType.FITS_FILE
# - FileType.TARGZ_FILE
# - FileType.PICKLE_FILE
# - FileType.JSON_FILE
# - FileType.CSV_FILE
# - FileType.ASCII_FILE
# imf_df[FileType.PNG_FILE] = os.path.join("/data", "io", "datafiles", "plot_image_IMF.png")
# imf_df[FileType.JPEG_FILE] = os.path.join("/data", "io", "datafiles", "plot_with_legend.jpg")
# imf_df[FileType.FITS_FILE] = os.path.join("/data", "io", "datafiles", "cassiopea_A_0.5-1.5keV.fits")
# imf_df[FileType.TARGZ_FILE] = os.path.join("/data", "io", "datafiles", "archive.tar.gz")
# imf_df[FileType.JSON_FILE] = os.path.join("/data", "io", "datafiles", "test_header_249.json")
# imf_df[FileType.ASCII_FILE] = os.path.join("/data", "io", "datafiles", "abstract.txt")
# imf_df[FileType.HDF5_FILE] = os.path.join("/data", "io", "HDF5", "study.h5")
# imf_df[FileType.PICKLE_FILE] = os.path.join("/data", "io", "datafiles", "dict_saved.pkl")

# Datafile plot information (for plot future updates and online interactive visualisation on Galactica web pages).
# Available plot types are :
# - LINE_PLOT
# - SCATTER_PLOT
# - HISTOGRAM
# - HISTOGRAM_2D
# - IMAGE
# - MAP_2D
# imf_df.plot_info = PlotInfo(plot_type=PlotType.LINE_PLOT, xaxis_values=N.array([10.0, 20.0, 30.0, 40.0, 50.0]),
#                             yaxis_values=N.array([1.256, 2.456, 3.921, 4.327, 5.159]), xaxis_log_scale=False,
#                             yaxis_log_scale=False, xaxis_label="Mass", yaxis_label="Probability", xaxis_unit=U.Msun,
#                             plot_title="Initial mass function", yaxis_unit=U.Mpc)
# -------------------------------------------------------------------------------------------------------------------- #

# ---------------------------------------- Target object => galaxy definition ---------------------------------------- #
# Target object properties have filter/sort flags to display them in various configurations on Galactica. Available
# flag values are :
# - PropertyFilterFlag.NO_FILTER
# - PropertyFilterFlag.BASIC_FILTER
# - PropertyFilterFlag.ADVANCED_FILTER

# - PropertySortFlag.NO_SORT
# - PropertySortFlag.BASIC_SORT
# - PropertySortFlag.ADVANCED_SORT
galaxy = TargetObject(name="Galaxy", description="Galaxy with spiral arms and sometimes bars")
x = galaxy.object_properties.add(ObjectProperty(property_name="xcenter", sort_flag=PropertySortFlag.BASIC_SORT,
                                                unit=comMpc, filter_flag=PropertyFilterFlag.ADVANCED_FILTER,
                                                #  dtype=DataType.REAL,
                                                description="Galaxy mass center coordinate along x-axis"))
y = galaxy.object_properties.add(ObjectProperty(property_name="ycenter", sort_flag=PropertySortFlag.BASIC_SORT,
                                                unit=comMpc, filter_flag=PropertyFilterFlag.ADVANCED_FILTER,
                                                # dtype=DataType.REAL,
                                                description="Galaxy mass center coordinate along y-axis"))
z = galaxy.object_properties.add(ObjectProperty(property_name="zcenter", sort_flag=PropertySortFlag.BASIC_SORT,
                                                unit=comMpc, filter_flag=PropertyFilterFlag.ADVANCED_FILTER,
                                                # dtype=DataType.REAL,
                                                description="Galaxy mass center coordinate along z-axis"))

# Property group (optional)
pos = galaxy.property_groups.add(ObjectPropertyGroup(group_name="center", description="galaxy center"))
pos.group_properties.add(x)
pos.group_properties.add(y)
pos.group_properties.add(z)

radius = galaxy.object_properties.add(ObjectProperty(property_name="r", sort_flag=PropertySortFlag.BASIC_SORT,
                                                     unit=comkpc, filter_flag=PropertyFilterFlag.ADVANCED_FILTER,
                                                     # dtype=DataType.REAL,
                                                     description="Galaxy radius"))
r_Mg_2 = galaxy.object_properties.add(ObjectProperty(property_name="$r_{\\textrm{M}_{g}/2}$", sort_flag=PropertySortFlag.BASIC_SORT,
                                                     unit=comkpc, filter_flag=PropertyFilterFlag.ADVANCED_FILTER,
                                                     # dtype=DataType.REAL,
                                                     description="Galaxy half-gas mass radius"))
r_e = galaxy.object_properties.add(ObjectProperty(property_name="$r_{e}$", sort_flag=PropertySortFlag.BASIC_SORT,
                                                  unit=comkpc, filter_flag=PropertyFilterFlag.ADVANCED_FILTER,
                                                  # dtype=DataType.REAL,
                                                  description="Galaxy half-stellar mass radius"))
tot_mass = galaxy.object_properties.add(ObjectProperty(property_name="$M_{\\textrm{tot}}$",
                                                       sort_flag=PropertySortFlag.BASIC_SORT,
                                                       unit=U.Msun, filter_flag=PropertyFilterFlag.ADVANCED_FILTER,
                                                       # dtype=DataType.REAL,
                                                       description="Galaxy total mass"))
stellar_mass = galaxy.object_properties.add(ObjectProperty(property_name="$M_{*}$", sort_flag=PropertySortFlag.BASIC_SORT,
                                                           unit=U.Msun, filter_flag=PropertyFilterFlag.ADVANCED_FILTER,
                                                           # dtype=DataType.REAL,
                                                           description="Galaxy stellar mass"))
ultra_compact = galaxy.object_properties.add(ObjectProperty(property_name="ultra compact", sort_flag=PropertySortFlag.BASIC_SORT,
                                                            filter_flag=PropertyFilterFlag.ADVANCED_FILTER,
                                                            dtype=DataType.BOOLEAN,
                                                            description="Is the galaxy ultra-compact ?"))
# -------------------------------------------------------------------------------------------------------------------- #


# ----------------------------------------------------- Result products ---------------------------------------------- #
# Target object catalog + field value arrays
cat_z4_descr = """<p>We detected galaxies with more than 50 stellar particles (about $10^{8} \; \Msun$) using AdaptaHOP 
(<a href="https://doi.org/10.1111/j.1365-2966.2004.07883.x" target="_blank">Aubert et al. 2004</a>). Here is an
interactive (rotating/zoomable) 3D scatterplot of the full galaxy catalog with point sizes scaling with the galaxy
total mass and point colors scaling with the stellar mass :</p>
<div>
    <script type="text/javascript">window.PlotlyConfig = {MathJaxConfig: 'local'};</script>
    <script src="https://cdn.plot.ly/plotly-2.17.1.min.js"></script>
        <div id="color_catalog_galaxy_z4" class="plotly-graph-div" style="height:1118px; width:1118px;"></div>
        <script type="text/javascript">
            window.PLOTLYENV=window.PLOTLYENV || {};
            if (document.getElementById("color_catalog_galaxy_z4")) {
                d3.json('/media/datafiles/products/cHJvZF81MzI4OA/cat_z4_colors.json', function(fig) {
                    Plotly.newPlot("color_catalog_galaxy_z4", fig.data, fig.layout, {'responsive': true});
                });
            };
        </script>
</div>"""
cat_z4 = sn_z4.catalogs.add(Catalog(target_object=galaxy, name="Galaxy catalog", description=cat_z4_descr))

# Import catalog data from JSON file
with open("./cat_z4_data.json", 'r') as fj:
    cat_z4_data = json.load(fj)

# Add field values
fx_z4 = CatalogField(obj_prop=x, values=N.array([float(v) for v in cat_z4_data["x_gal"].values()], dtype='f'))
cat_z4.catalog_fields.add(fx_z4)
fy_z4 = CatalogField(obj_prop=y, values=N.array([float(v) for v in cat_z4_data["y_gal"].values()], dtype='f'))
cat_z4.catalog_fields.add(fy_z4)
fz_z4 = CatalogField(obj_prop=z, values=N.array([float(v) for v in cat_z4_data["z_gal"].values()], dtype='f'))
cat_z4.catalog_fields.add(fz_z4)
frad_z4 = CatalogField(obj_prop=radius, values=N.array([float(v) for v in cat_z4_data["radius"].values()], dtype='f'))
cat_z4.catalog_fields.add(frad_z4)
fr_Mg_2_z4 = CatalogField(obj_prop=r_Mg_2, values=N.array([float(v) for v in cat_z4_data["half-gas-mass_radius"].values()], dtype='f'))
cat_z4.catalog_fields.add(fr_Mg_2_z4)
ftot_mass_z4 = CatalogField(obj_prop=tot_mass, values=N.array([float(v) for v in cat_z4_data["total_mass"].values()], dtype='f'))
cat_z4.catalog_fields.add(ftot_mass_z4)
fstel_mass_z4 = CatalogField(obj_prop=stellar_mass, values=N.array([float(v) for v in cat_z4_data["stellar_mass"].values()], dtype='f'))
cat_z4.catalog_fields.add(fstel_mass_z4)

# Add datafiles to the catalog if required.
cdf1_z4 = cat_z4.datafiles.add(Datafile(name="Catalog 3D scatterplot data",
description="This is a 3D scatterplot JSON file for interactive rendering of the galaxy 3D coordinates in the simulation box"))
cdf1_z4[FileType.JSON_FILE] = "./cat_z4_colors.json"

# Datafile for z=3 snapshot
sn_z3_df2 = sn_z3.datafiles.add(Datafile(name="Gas density in the CGM and IGM around a massive halo in the Extreme Horizon and Standard Horizon simulations in the same region.",
                                         description="""Projected density (left) and physical resolution (right) in Extreme Horizon (top) and Standard Horizon (bottom) zoomed on a massive halo
at $z=3$. The depth of the projections are $200 \; \\textrm{kpc}\cdot\\textrm{h}^{-1}$ and the boxes extend to $1 \; \\textrm{Mpc}\cdot\\textrm{h}^{-1}$ on each side.
The gas density is computed as the mass-weighted average of local densities along the line of sight corresponding to each pixel.
The resolution shown is the resolution of the cell in which the gas density is the highest along each line of sight."""))
sn_z3_df2[FileType.PNG_FILE] = "./aa38614-20-fig7.png"


cat_z3_descr = """<p>We detected galaxies with more than 50 stellar particles (about $10^{8} \; \Msun$) using AdaptaHOP 
(<a href="https://doi.org/10.1111/j.1365-2966.2004.07883.x" target="_blank">Aubert et al. 2004</a>). Here is an
interactive (rotating/zoomable) 3D scatterplot of the full galaxy catalog with point sizes scaling with the galaxy
total mass and point colors scaling with the stellar mass :</p>
<div>
    <script type="text/javascript">window.PlotlyConfig = {MathJaxConfig: 'local'};</script>
    <script src="https://cdn.plot.ly/plotly-2.17.1.min.js"></script>
        <div id="color_catalog_galaxy_z3" class="plotly-graph-div" style="height:1118px; width:1118px;"></div>
        <script type="text/javascript">
            window.PLOTLYENV=window.PLOTLYENV || {};
            if (document.getElementById("color_catalog_galaxy_z3")) {
                d3.json('/media/datafiles/products/cHJvZF81MzI4OQ/cat_z3_colors.json', function(fig) {
                    Plotly.newPlot("color_catalog_galaxy_z3", fig.data, fig.layout, {'responsive': true});
                });
            };
        </script>
</div>"""
cat_z3 = sn_z3.catalogs.add(Catalog(target_object=galaxy, name="Galaxy catalog", description=cat_z3_descr))

# Import catalog data from JSON file
with open("./cat_z3_data.json", 'r') as fj:
    cat_z3_data = json.load(fj)

# Add field values
fx_z3 = CatalogField(obj_prop=x, values=N.array([float(v) for v in cat_z3_data["x_gal"].values()], dtype='f'))
cat_z3.catalog_fields.add(fx_z3)
fy_z3 = CatalogField(obj_prop=y, values=N.array([float(v) for v in cat_z3_data["y_gal"].values()], dtype='f'))
cat_z3.catalog_fields.add(fy_z3)
fz_z3 = CatalogField(obj_prop=z, values=N.array([float(v) for v in cat_z3_data["z_gal"].values()], dtype='f'))
cat_z3.catalog_fields.add(fz_z3)
frad_z3 = CatalogField(obj_prop=radius, values=N.array([float(v) for v in cat_z3_data["radius"].values()], dtype='f'))
cat_z3.catalog_fields.add(frad_z3)
fr_Mg_2_z3 = CatalogField(obj_prop=r_Mg_2, values=N.array([float(v) for v in cat_z3_data["half-gas-mass_radius"].values()], dtype='f'))
cat_z3.catalog_fields.add(fr_Mg_2_z3)
ftot_mass_z3 = CatalogField(obj_prop=tot_mass, values=N.array([float(v) for v in cat_z3_data["total_mass"].values()], dtype='f'))
cat_z3.catalog_fields.add(ftot_mass_z3)
fstel_mass_z3 = CatalogField(obj_prop=stellar_mass, values=N.array([float(v) for v in cat_z3_data["stellar_mass"].values()], dtype='f'))
cat_z3.catalog_fields.add(fstel_mass_z3)

# Add datafiles to the catalog if required.
cdf1_z3 = cat_z3.datafiles.add(Datafile(name="Catalog 3D scatterplot data",
description="This is a 3D scatterplot JSON file for interactive rendering of the galaxy 3D coordinates in the simulation box"))
cdf1_z3[FileType.JSON_FILE] = "./cat_z3_colors.json"


cat_z2_descr = """<p>We detected galaxies with more than 50 stellar particles (about $10^{8} \; \Msun$) using AdaptaHOP 
(<a href="https://doi.org/10.1111/j.1365-2966.2004.07883.x" target="_blank">Aubert et al. 2004</a>).
There are 37 698 galaxies detected in Extreme Horizon at $z\sim2$ and 20 314 in Standard Horizon.
While the mass functions above $10^{10} \; \Msun$ are quite similar in both simulations, Extreme Horizon forms twice as
many galaxies as Standard Horison with stellar mass $\\textrm{M}_{*} \le 5\\times 10^{9} \; \Msun$. We rule out any detection
bias since stellar particles have similar masses in both simulations (new stars form at the maximal resolution level in each
simulation) and we attribute this difference to the increased resolution in low-density regions.</p>

<p>Here is an interactive (rotating/zoomable) 3D scatterplot of the full galaxy catalog with point sizes
scaling with the galaxy total mass and point colors scaling with the stellar mass :</p>
<div>
    <script type="text/javascript">window.PlotlyConfig = {MathJaxConfig: 'local'};</script>
    <script src="https://cdn.plot.ly/plotly-2.17.1.min.js"></script>
        <div id="color_catalog_galaxy_z2" class="plotly-graph-div" style="height:1118px; width:1118px;"></div>
        <script type="text/javascript">
            window.PLOTLYENV=window.PLOTLYENV || {};
            if (document.getElementById("color_catalog_galaxy_z2")) {
                d3.json('/media/datafiles/products/cHJvZF81MzI4Ng/cat_z2_colors.json', function(fig) {
                    Plotly.newPlot("color_catalog_galaxy_z2", fig.data, fig.layout, {'responsive': true});
                });
            };
        </script>
</div>"""
cat_z2 = sn_z2.catalogs.add(Catalog(target_object=galaxy, name="Galaxy catalog", description=cat_z2_descr))


# Import catalog data from JSON file
with open("./cat_z2_data.json", 'r') as fj:
    cat_z2_data = json.load(fj)

# Add field values
fx = cat_z2.catalog_fields.add(CatalogField(obj_prop=x,
                                            values=N.array([float(v) for v in cat_z2_data["x_gal"].values()], dtype='f')))
fy = cat_z2.catalog_fields.add(CatalogField(obj_prop=y,
                                            values=N.array([float(v) for v in cat_z2_data["y_gal"].values()], dtype='f')))
fz = cat_z2.catalog_fields.add(CatalogField(obj_prop=z,
                                            values=N.array([float(v) for v in cat_z2_data["z_gal"].values()], dtype='f')))
frad = cat_z2.catalog_fields.add(CatalogField(obj_prop=radius,
                                              values=N.array([float(v) for v in cat_z2_data["radius"].values()], dtype='f')))
fr_Mg_2 = cat_z2.catalog_fields.add(CatalogField(obj_prop=r_Mg_2,
                                                 values=N.array([float(v) for v in cat_z2_data["half-gas-mass_radius"].values()], dtype='f')))
ftot_mass = cat_z2.catalog_fields.add(CatalogField(obj_prop=tot_mass,
                                                   values=N.array([float(v) for v in cat_z2_data["total_mass"].values()], dtype='f')))
fstel_mass = cat_z2.catalog_fields.add(CatalogField(obj_prop=stellar_mass,
                                                    values=N.array([float(v) for v in cat_z2_data["stellar_mass"].values()], dtype='f')))
fuc = cat_z2.catalog_fields.add(CatalogField(obj_prop=ultra_compact,
                                             values=N.array(list(cat_z2_data["ultra_compact"].values()), dtype='b')))


# Add datafiles to the catalog if required.
cdf1 = cat_z2.datafiles.add(Datafile(name="Catalog 3D scatterplot data",
description="This is a 3D scatterplot JSON file for interactive rendering of the galaxy 3D coordinates in the simulation box"))
cdf1[FileType.JSON_FILE] = "./cat_z2_colors.json"

# Add
sn_z2_df1 = sn_z2.datafiles.add(Datafile(name="Large-scale structure of the Extreme Horizon simulation at redshift $z = 2$",
                                         description="""Projected map of the EH simulation at $z \simeq 2$.
Gas density (grey), entropy (red), and metallicity (green) are shown."""))
sn_z2_df1[FileType.PNG_FILE] = "./aa38614-20-fig6.png"


# Ultra-compact galaxies sub-catalog
cat_z2_UC_descr = """<p>83% of the galaxies in the full galaxy catalog are on the main sequence of star formation 
(<a href="https://doi.org/10.1051/0004-6361/201117239" target="_blank">Elbaz et al. 2011</a>),
so that we can compare their size to the model from <a target="_blank" href="https://doi.org/10.1111/j.1365-2966.2010.17555.x">Dutton et al. (2011)</a>,
which is known to provide a good fit to MS galaxies at $z = 2$ (footnote 1). The Standard Horizon galaxies are larger
than both the Extreme Horizon galaxies and observed main sequence galaxies. The Extreme Horizon galaxies generally lie
around the observed relation and a small fraction of them have significantly smaller sizes.
We define the compactness, $\mathscr{C}$, as the ratio between the radius expected from the 
<a target="_blank" href="https://doi.org/10.1111/j.1365-2966.2010.17555.x">Dutton et al. (2011)</a> model and the actual
radius. The compactness distribution for Extreme Horizon (Fig. 3) peaks at around $\mathscr{C} \simeq 1$ but it exhibits
a distinct tail for $\mathscr{C} \gt 1.3$. We thus define two massive galaxy populations in Extreme Horizon:
10 ultra-compact (UC) galaxies with $\mathscr{C} \gt 1.3$ and 50 non ultra-compact (NUC) ones.</p>

<p>Here is an interactive (rotating/zoomable) 3D scatterplot of the full galaxy catalog with point sizes
scaling with the galaxy total mass and point colors highlighting the ultra-compact ones (in red) :</p>
<div>
    <script type="text/javascript">window.PlotlyConfig = {MathJaxConfig: 'local'};</script>
    <script src="https://cdn.plot.ly/plotly-2.17.1.min.js"></script>
        <div id="catalog_UC_galaxy_z2" class="plotly-graph-div" style="height:1118px; width:1118px;"></div>
        <script type="text/javascript">
            window.PLOTLYENV=window.PLOTLYENV || {};
            if (document.getElementById("catalog_UC_galaxy_z2")) {
                d3.json('/media/datafiles/products/cHJvZF81MzI4Nw/cat_z2_UC.json', function(fig) {
                    Plotly.newPlot("catalog_UC_galaxy_z2", fig.data, fig.layout, {'responsive': true});
                });
            };
        </script>
</div>"""
cat_z2_UC = sn_z2.catalogs.add(Catalog(target_object=galaxy, name="Ultra-compact galaxy catalog", description=cat_z2_UC_descr))


# Import catalog data from JSON file
with open("./cat_z2_UC_data.json", 'r') as fj:
    cat_z2_UC_data = json.load(fj)

# Add field values
fx_UC = cat_z2_UC.catalog_fields.add(CatalogField(obj_prop=x,
                                                  values=N.array([float(v) for v in cat_z2_UC_data["x_gal"].values()], dtype='f')))
fy_UC = cat_z2_UC.catalog_fields.add(CatalogField(obj_prop=y,
                                                  values=N.array([float(v) for v in cat_z2_UC_data["y_gal"].values()], dtype='f')))
fz_UC = cat_z2_UC.catalog_fields.add(CatalogField(obj_prop=z,
                                                  values=N.array([float(v) for v in cat_z2_UC_data["z_gal"].values()], dtype='f')))
frad_UC = cat_z2_UC.catalog_fields.add(CatalogField(obj_prop=radius,
                                                    values=N.array([float(v) for v in cat_z2_UC_data["radius"].values()], dtype='f')))
fre_UC = cat_z2_UC.catalog_fields.add(CatalogField(obj_prop=r_e,
                                                   values=N.array([float(v) for v in cat_z2_UC_data["r_e"].values()], dtype='f')))
fr_Mg_2_UC = cat_z2_UC.catalog_fields.add(CatalogField(obj_prop=r_Mg_2,
                                                       values=N.array([float(v) for v in cat_z2_UC_data["half-gas-mass_radius"].values()], dtype='f')))
ftot_mass_UC = cat_z2_UC.catalog_fields.add(CatalogField(obj_prop=tot_mass,
                                                         values=N.array([float(v) for v in cat_z2_UC_data["total_mass"].values()], dtype='f')))
fstel_mass_UC = cat_z2_UC.catalog_fields.add(CatalogField(obj_prop=stellar_mass,
                                                          values=N.array([float(v) for v in cat_z2_UC_data["stellar_mass"].values()], dtype='f')))


# Add datafiles to the catalog if required.
cdf_UC = cat_z2_UC.datafiles.add(Datafile(name="Catalog 3D scatterplot data",
description="This is a 3D scatterplot JSON file for interactive rendering of the galaxy 3D coordinates in the simulation box"))
cdf_UC[FileType.JSON_FILE] = "./cat_z2_UC.json"

cdf2_UC = cat_z2_UC.datafiles.add(Datafile(name="Stellar masses and radii",
description="""Stellar half-mass radius $\\textrm{R}_{e}$ versus stellar mass $\\textrm{M}_{*}$ for massive galaxies at
$z=2$ in Extreme Horizon and Standard Horizon. The displayed model from <a href="https://doi.org/10.1111/j.1365-2966.2010.17555.x" target="_blank">Dutton et al. (2011)</a>
provides a good fit to SFGs at $z=2$. UC galaxies lie below the black dashed line while NUC galaxies are above. We identify
Extreme Horizon galaxies above and below the Main Sequence of star formation with stars and triangles, respectively,
following the definition of the Main Sequence from <a target="_blank" href="https://doi.org/10.1051/0004-6361/201629155">Schreiber et al. (2017)</a>."""))
cdf2_UC[FileType.PNG_FILE] = "./aa38614-20-fig2.png"
# -------------------------------------------------------------------------------------------------------------------- #


# ---------------------------------------------- Data processing services bindings ----------------------------------- #
# # Add data processing services to a snapshot
# dps1 = sn.processing_services.add((DataProcessingService(service_name="column_density_map",
#                                                          data_host="MyDeptCluster")))
# dps2 = sn2.processing_services.add((DataProcessingService(service_name="ppv_cube", data_host="Lab_GPU_cluster")))
#
# # Add data processing services to a target object catalog
# dps3 = CatalogDataProcessingService(service_name="slice_map", data_host="Lab_Cluster")
# cat.processing_services.add(dps3)
# # Define catalog field bindings to automatically fill the data processing service parameter value 'pv' with a catalog
# # field value 'fv' of one of your catalog's object according to the formula : pv = scale * fv + offset. Default scale
# # value is 1.0 and default offset is 0.0.
# fbx = dps1.catalog_field_bindings.add(CatalogFieldBinding(catalog_field=fx, param_key="x",
#                                                           scale=1.0e2, offset=-50.0))
# fby = dps1.catalog_field_bindings.add(CatalogFieldBinding(catalog_field=fy, param_key="y",
#                                                           scale=1.0e2, offset=-50.0))
# fbz = dps1.catalog_field_bindings.add(CatalogFieldBinding(catalog_field=fz, param_key="z",
#                                                           scale=1.0e2, offset=-50.0))

# -------------------------------------------------------------------------------------------------------------------- #


# Save study in HDF5 file
study = SimulationStudy(project=proj)
study.save_HDF5("./EH_study.h5", galactica_checks=True)

# Eventually reload it from HDF5 file to edit its content
# study = SimulationStudy.load_HDF5("./EH_study.h5")