#!/usr/bin/env python
import os
import numpy as N

from astrophysix.simdm import SimulationStudy, Project, ProjectCategory
from astrophysix.simdm.catalogs import TargetObject, ObjectProperty, PropertySortFlag, PropertyFilterFlag, \
    ObjectPropertyGroup, Catalog, CatalogField
from astrophysix.simdm.experiment import Simulation, AppliedAlgorithm, ParameterSetting, ParameterVisibility,\
    ResolvedPhysicalProcess
from astrophysix.simdm.protocol import SimulationCode, AlgoType, Algorithm, InputParameter, PhysicalProcess, Physics
from astrophysix.simdm.results import GenericResult, Snapshot
from astrophysix.simdm.datafiles import Datafile, PlotType, PlotInfo, AsciiFile
from astrophysix.simdm.services import DataProcessingService, CatalogDataProcessingService, CatalogFieldBinding
from astrophysix.simdm.utils import DataType
from astrophysix.utils.file import FileType
from astrophysix import units as U

# # Eventually reload it from HDF5 file to edit its content
# study = SimulationStudy.load_HDF5("./obelisk_study.h5")
# exit(0)

# ----------------------------------------------- Project creation --------------------------------------------------- #
proj = Project(category=ProjectCategory.Cosmology, project_title="Obelisk",
               alias="OBELISK", short_description="Short description of my the Obelisk cosmological simulation project.",
               general_description="""<p>Massive galaxies assembled most of their mass in the first 3 Gyr of evolution of 
               the Universe, before the peak of cosmic star formation. Supermassive black holes powering bright quasars 
               are often found at the centre of these massive galaxies. The intense star formation and the accretion onto
               central supermassive black holes release a tremendous amount of energy in the surrounding gas via various
               feedback channels, whether radiative or hydrodynamical, strongly shaping the gas flows in and around
               galaxies, and will affect all neighbouring galaxies. On larger scales, the intense ultra-violet radiation
               produced by these massive stars and active galactic nuclei reionized the hydrogen in the intergalactic
               medium by $z\simeq 6$.</p>
               <p><img src="https://obelisk-simulation.github.io/img/Obelisk_cross_clean_highlight_annotated.jpg" style="width:100%;"></p>
               <p>To study this fascinating epoch, we introduce the Obelisk project: a radiation-hydrodynamical
               cosmological simulation following the assembly of a proto-cluster and its environment until $z \simeq 3$.
               Obelisk relies on state-of-the-art models to describe the physical processes necessary to simulate
               galaxies and their environment (see the details here). Re-simulating at very high-resolution (34 pc) a
               region of the large-scale Horizon-AGN simulation, Obelisk simulates the first 2 Gyr of the assembly o
                what will become a cluster of total mass $M_{halo} \simeq 7\\times 10^{14} M_\odot$, similar to Coma
                cluster, with a resolution high enough to capture the multi-phase structure of the interstellar medium
                of the 67 500 galaxies identified $z \simeq 3.5$.</p>""",
               data_description="The data available in this project...", directory_path="/path/to/project/data/on/IAP/cluster/",
               acknowledgement="""<p>
               <div>You may acknowledge this project using the following reference :<pre>
               @ARTICLE{2021A&A...653A.154T,
               author = {{Trebitsch}, Maxime and {Dubois}, Yohan and {Volonteri}, Marta and {Pfister}, Hugo and {Cadiou}, Corentin and {Katz}, Harley and {Rosdahl}, Joakim and {Kimm}, Taysun and {Pichon}, Christophe and {Beckmann}, Ricarda S. and {Devriendt}, Julien and {Slyz}, Adrianne},
               title = "{The OBELISK simulation: Galaxies contribute more than AGN to H I reionization of protoclusters}",
               journal = {\\aap},
               keywords = {methods: numerical, galaxies: formation, galaxies: high-redshift, intergalactic medium, quasars: supermassive black holes, dark ages, reionization, first stars, Astrophysics - Astrophysics of Galaxies, Astrophysics - Cosmology and Nongalactic Astrophysics},
               year = 2021,
               month = sep,
               volume = {653},
               eid = {A154},
               pages = {A154},
               doi = {10.1051/0004-6361/202037698},
               archivePrefix = {arXiv},
               eprint = {2002.04045},
               primaryClass = {astro-ph.GA},
               adsurl = {https://ui.adsabs.harvard.edu/abs/2021A&A...653A.154T},
               adsnote = {Provided by the SAO/NASA Astrophysics Data System}}
               </pre></div></p>""",
               thumbnail_image="./Obelisk_allvert_small.jpg")
# Set the project into a study (for HDF5 I/O)
study = SimulationStudy(project=proj)
# -------------------------------------------------------------------------------------------------------------------- #


# --------------------------------------- Simulation code definition ------------------------------------------------- #
ramses = SimulationCode(name="Ramses-RT", code_name="Ramses_RT", code_version="3.10.1", alias="RAMSES_RT",
                        url="https://www.ics.uzh.ch/~teyssier/ramses/RAMSES.html",
                        description="""This is a fair description of the Ramses code.
                        For RAMSES-RT, ...""")

# => Add algorithms
amr = ramses.algorithms.add(Algorithm(algo_type=AlgoType.AdaptiveMeshRefinement, description="AMR descr"))
ramses.algorithms.add(Algorithm(algo_type=AlgoType.Godunov, description="Godunov scheme"))
ramses.algorithms.add(Algorithm(algo_type=AlgoType.HLLCRiemann, description="HLLC Riemann solver"))
ramses.algorithms.add(Algorithm(algo_type=AlgoType.PoissonMultigrid, description="Multigrid Poisson solver"))
ramses.algorithms.add(Algorithm(algo_type=AlgoType.RadiativeTransfer, description="Radiative transfer"))
ramses.algorithms.add(Algorithm(algo_type=AlgoType.ParticleMesh, description="PM solver"))

# => Add input parameters
lmin = ramses.input_parameters.add(InputParameter(key="levelmin", name="$l_{\\textrm{min}}$",
                                                  description="min. level of AMR refinement"))
lmax = ramses.input_parameters.add(InputParameter(key="levelmax", name="$l_{\\textrm{max}}$",
                                                  description="max. level of AMR refinement"))
boxlen = ramses.input_parameters.add(InputParameter(key="boxlen", name="$L_{\\textrm{box}}$",
                                                    description="size of the simulation domain"))

# => Add physical processes : available physics are :
# - Physics.SelfGravity
# - Physics.ExternalGravity
# - Physics.Hydrodynamics
# - Physics.MHD
# - Physics.StarFormation
# - Physics.SupernovaeFeedback
# - Physics.AGNFeedback
# - Physics.SupermassiveBlackHoleFeedback
# - Physics.StellarIonisingRadiation
# - Physics.StellarUltravioletRadiation
# - Physics.StellarInfraredRadiation
# - Physics.ProtostellarJetFeedback
# - Physics.Chemistry
# - Physics.AtomicCooling
# - Physics.DustCooling
# - Physics.MolecularCooling
# - Physics.TurbulentForcing
# - Physics.RadiativeTransfer

ramses.physical_processes.add(PhysicalProcess(physics=Physics.StarFormation, description="description sf"))
ramses.physical_processes.add(PhysicalProcess(physics=Physics.Hydrodynamics, description="description hydro"))
grav = ramses.physical_processes.add(PhysicalProcess(physics=Physics.SelfGravity, description="description self-gravity"))
ramses.physical_processes.add(PhysicalProcess(physics=Physics.SupernovaeFeedback, description="SN feedback"))
ramses.physical_processes.add(PhysicalProcess(physics=Physics.RadiativeTransfer, description="Radiative transfer process"))
# -------------------------------------------------------------------------------------------------------------------- #


# -------------------------------------------- Simulation setup ------------------------------------------------------ #
simu = Simulation(simu_code=ramses, name="Obelisk", alias="OBELISK",
                  description="""<p>The initial conditions for the Obelisk simulation were chosen to follow the high-redshift
                  evolution of an overdense environment. For this purpose, we chose to re-simulate with a high resolution
                  the region around the most massive halo at $z\sim 2$ in the Horizon-AGN simulation: selecting initial
                  conditions based on this simulation allows our results to be compared and contrasted with the work that
                  has already resulted from Horizon-AGN.</p>
                  
                  The Horizon-AGN simulation follows the evolution of a cosmological volume of side $L_{box} = 100\, h^{-1} cMpc$
                  (and periodic boundary conditions), assuming a $\Lambda$CDM cosmology compatible with the 7-year 
                  data from the Wilkinson Microwave Anistropy Probe (Komatsu et al. 2011): Hubble constant 
                  $H_0 = 70.4\,\mbox{km}\,\mbox{s}^{-1}\,\mbox{Mpc}^{-1}$, total matter density $\Omega_{m} = 0.272$,
                  dark energy density $\Omega_{\Lambda} = 0.728$, baryon density $\Omega_{b} = 0.0455$, amplitude of the
                  matter power spectrum $\sigma_8 = 0.81$, and scalar spectral index $n_s = 0.967$. The initial conditions
                  have been created with MPgrafic (Prunet et al. 2008; Prunet & Pichon 2013) with $10243^3$ DM particles
                  and as many gas cells (corresponding to a DM mass resolution of $M_{DM,HR} = 8 \\times 10^7 \, M_\odot$).
                  In the original Horizon-AGN run, the grid is then adaptively refined over the course of the simulation,
                  maintaining a maximum spatial resolution of $\Delta x = 1$ proper kpc at all redshifts down to $z=0$.
                  We improve on this resolution by a factor of $\simeq 30$ in our Obelisk sub-volume, reaching a
                  resolution of $\Delta x = 35 \; \\textrm{pc}$ (more details).</p>
                  
                  <p>In order to achieve our target resolution at a reasonable computation cost, we only re-simulated a
                  fraction of the initial Horizon-AGN volume. To this end, we selected the most massive halo (of virial
                  mass is around $M_{vir} \simeq 2.5 \\times 10^{13}\,M_\odot$) in the simulation at $z \sim 2$ as
                  identified by the [AdaptaHOP]{smallcaps} halo finder (Aubert et al. 2004; Tweed et al. 2009). By $z = 0$,
                  this halo remains the most massive cluster of Horizon-AGN, with $M_{vir} \sim 6.6 \\times 10^{14}\,M_\odot$.
                  We first identified all the particles within $4 R_{vir}$ of the target halo at $z = 1.97$, namely in a
                  sphere of radius $2.51\,h^{-1} \mbox{cMpc}$ and tracked them back to the initial conditions. We computed
                  the convex hull enclosing all these particles in the initial conditions and defined this region as
                  our high-resolution patch. We then created a re-sampled version of the Horizon-AGN initial conditions
                  with $4096^3$ DM particles, corresponding to a mass resolution of $M_{DM,HR} = 1.2\\times 10^6\,M_\odot$.
                  Finally, we selected all the high-resolution particles belonging to the patch previously defined, and
                  embedded this patch in the larger Horizon-AGN box, using successively lower and lower resolution regions
                  as buffers, until reaching an effective resolution of $256^3$ particles in the outer parts of the volume.
                  We filled the full volume with a passive variable whose value is 1 within the high-resolution patch and
                  0 outside, and used this as a refinement mask (more details). The figure below illustrates the gain in
                  resolution between Horizon-AGN (upper row) and Obelisk (lower row).</p>
                  <p><img src="https://obelisk-simulation.github.io/img/HAGN_vs_Obelisk_all.jpg" style="width:100%;"></p>
                  <p>Finally, the Obelisk simulation improves upon its parent Horizon-AGN in several important ways
                  (beyond resolution) which we describe in detail further down.</p>
                  <p>A similar methodology has been employed for the New-Horizon simulation, which focuses on an average
                  region of the Universe. Apart from the radiation-hydrodynamical evolution, our numerical methodology
                  is kept as close as possible to the New-Horizon simulation, so as to facilitate the comparison between
                  the two simulations.</p>""",
                  execution_time="2020-03-01 18:45:30", # config_file="/path/to/namelists/obelisk.nml",  # If you want it to be available for download
                  directory_path="/path/to/simu/data/on/IAP/cluster/obelisk")
proj.simulations.add(simu)


# Add applied algorithms implementation details. Warning : corresponding algorithms must have been added in the 'ramses'
# simulation code.
simu.applied_algorithms.add(AppliedAlgorithm(algorithm=amr, details="AMR octree implementation as described in [Teyssier 2002]"))
simu.applied_algorithms.add(AppliedAlgorithm(algorithm=ramses.algorithms[AlgoType.HLLCRiemann.name],
                                             details="HLLC Riemann solver implementation as detailed in [Teyssier 2002]"))
simu.applied_algorithms.add(AppliedAlgorithm(algorithm=ramses.algorithms[AlgoType.RadiativeTransfer.name],
                                             details="""Radative tranfer algorithm implementation in RAMSES-RT..."""))


# Add parameter setting. Warning : corresponding input parameter must have been added in the 'ramses' simulation code.
# Available parameter visibility options are :
# - ParameterVisibility.NOT_DISPLAYED
# - ParameterVisibility.ADVANCED_DISPLAY
# - ParameterVisibility.BASIC_DISPLAY
simu.parameter_settings.add(ParameterSetting(input_param=lmin, value=10,
                                             visibility=ParameterVisibility.BASIC_DISPLAY))
simu.parameter_settings.add(ParameterSetting(input_param=lmax, value=19,
                                             visibility=ParameterVisibility.BASIC_DISPLAY))
# Create a user-custom unit to define the simulation domain size in comobile length unit
comMpc = U.Unit.create_unit(name="Mpc_h-1", base_unit=U.Mpc/0.76, descr="comobile Mpc", latex="$h^{-1} \, \\textrm{cMpc}$")
simu.parameter_settings.add(ParameterSetting(input_param=boxlen, value=100, unit=comMpc,
                                             visibility=ParameterVisibility.BASIC_DISPLAY))


# Add resolved physical process implementation details. Warning : corresponding physical process must have been added to
# the 'ramses' simulation code
simu.resolved_physics.add(ResolvedPhysicalProcess(physics=ramses.physical_processes[Physics.RadiativeTransfer.name],
                                                  details="Radiative transfer physical model details"))
simu.resolved_physics.add(ResolvedPhysicalProcess(physics=grav, details="self-gravity specific implementation"))
# -------------------------------------------------------------------------------------------------------------------- #


# -------------------------------------- Simulation generic result and snapshots ------------------------------------- #
# Generic result
# gres = GenericResult(name="", description="My description", directory_path="/my/path/to/result")
# simu.generic_results.add(gres)

# Simulation snapshot
sn = simu.snapshots.add(Snapshot(name="$z \simeq 3.0$", time=(-6.5, U.Myr), physical_size=(100.0, U.Mpc),
                                 description="""Snapshot at redshift $z \simeq 3.0$""",
                                 directory_path="/path/to/obelisk/run/outputs/", data_reference="output_00075"))
# -------------------------------------------------------------------------------------------------------------------- #


# ---------------------------------------------------- Result datafiles ---------------------------------------------- #
# Datafile creation
# imf_df = sn.datafiles.add(Datafile(name="Initial mass function plot",
#                                    description="This is my plot detailed description"))

# Add attached files to a datafile (1 per file type). Available file types are :
# - FileType.HDF5_FILE
# - FileType.PNG_FILE
# - FileType.JPEG_FILE
# - FileType.FITS_FILE
# - FileType.TARGZ_FILE
# - FileType.PICKLE_FILE
# - FileType.JSON_FILE
# - FileType.CSV_FILE
# - FileType.ASCII_FILE
# imf_df[FileType.PNG_FILE] = os.path.join("/data", "io", "datafiles", "plot_image_IMF.png")
# imf_df[FileType.JPEG_FILE] = os.path.join("/data", "io", "datafiles", "plot_with_legend.jpg")
# imf_df[FileType.FITS_FILE] = os.path.join("/data", "io", "datafiles", "cassiopea_A_0.5-1.5keV.fits")
# imf_df[FileType.TARGZ_FILE] = os.path.join("/data", "io", "datafiles", "archive.tar.gz")
# imf_df[FileType.JSON_FILE] = os.path.join("/data", "io", "datafiles", "test_header_249.json")
# imf_df[FileType.ASCII_FILE] = os.path.join("/data", "io", "datafiles", "abstract.txt")
# imf_df[FileType.HDF5_FILE] = os.path.join("/data", "io", "HDF5", "study.h5")
# imf_df[FileType.PICKLE_FILE] = os.path.join("/data", "io", "datafiles", "dict_saved.pkl")

# Datafile plot information (for plot future updates and online interactive visualisation on Galactica web pages).
# Available plot types are :
# - LINE_PLOT
# - SCATTER_PLOT
# - HISTOGRAM
# - HISTOGRAM_2D
# - IMAGE
# - MAP_2D
# imf_df.plot_info = PlotInfo(plot_type=PlotType.LINE_PLOT, xaxis_values=N.array([10.0, 20.0, 30.0, 40.0, 50.0]),
#                             yaxis_values=N.array([1.256, 2.456, 3.921, 4.327, 5.159]), xaxis_log_scale=False,
#                             yaxis_log_scale=False, xaxis_label="Mass", yaxis_label="Probability", xaxis_unit=U.Msun,
#                             plot_title="Initial mass function", yaxis_unit=U.Mpc)
# -------------------------------------------------------------------------------------------------------------------- #


# ------------------------------ Target object definitions (+ properties/property groups)  --------------------------- #
# Target object properties have filter/sort flags to display them in various configurations on Galactica. Available
# flag values are :
# - PropertyFilterFlag.NO_FILTER
# - PropertyFilterFlag.BASIC_FILTER
# - PropertyFilterFlag.ADVANCED_FILTER

# - PropertySortFlag.NO_SORT
# - PropertySortFlag.BASIC_SORT
# - PropertySortFlag.ADVANCED_SORT
halo = TargetObject(name="Halo",
                    description="""<p>In the current version, the catalogue only focuses in "pure" haloes, defined as
                    those identified by the halofinder with a mass fraction of "contaminated" DM particles (i.e. 
                    high-mass, low-resolution) of 0%.</p>
                    
                    <p>Furthermore, objects are eligible to be in the catalogue if and only if they are considered as
                    resolved galaxies, i.e. objects with at least 100 DM particles (i.e. a DM halo) and 100 star
                    particles (i.e. a galaxy). This should select all resolved galaxies, and the DM requirement should
                    prevent the inclusion of stellar clumps.</p>
                    
                    <p>The BH matching includes only the "central" BH, and some galaxies will not have one. In this 
                    case, the BH fields should have a NaN value.</p>""")

id_halo = ObjectProperty(property_name="ID", sort_flag=PropertySortFlag.ADVANCED_SORT,
                         filter_flag=PropertyFilterFlag.BASIC_FILTER, dtype=DataType.INTEGER,
                         description="ID of each halo")
halo.object_properties.add(id_halo)

halo_level = ObjectProperty(property_name="level", sort_flag=PropertySortFlag.BASIC_SORT,
                            filter_flag=PropertyFilterFlag.BASIC_FILTER, dtype=DataType.INTEGER,
                            description="""Level of the halofinder structure. 1 is "top-level", 2 is a substructure, etc.
                            Note that a substructure can still be a "central" galaxy: AdaptaHOP levels reflect whether
                            objects are within the "radius" of a parent, rather than inside e.g. the virial radius.""")
halo.object_properties.add(halo_level)

within_host = ObjectProperty(property_name="within_host", sort_flag=PropertySortFlag.BASIC_SORT,
                             filter_flag=PropertyFilterFlag.BASIC_FILTER, dtype=DataType.BOOLEAN,
                             description="""Whether the object is within the virial radius of its top-level parent, if
                             level > 1.""")
halo.object_properties.add(within_host)

within_parent = ObjectProperty(property_name="within_parent", sort_flag=PropertySortFlag.BASIC_SORT,
                               filter_flag=PropertyFilterFlag.BASIC_FILTER, dtype=DataType.BOOLEAN,
                               description="""Whether the object is within the virial radius of its direct parent, if 
                               level > 1.""")
halo.object_properties.add(within_parent)

is_central = ObjectProperty(property_name="is_central", sort_flag=PropertySortFlag.BASIC_SORT,
                            filter_flag=PropertyFilterFlag.BASIC_FILTER, dtype=DataType.BOOLEAN,
                            description="""Simple flag for central objects (i.e. neither within their direct or
                            top-level parent).""")
halo.object_properties.add(is_central)

host = ObjectProperty(property_name="host", sort_flag=PropertySortFlag.BASIC_SORT,
                      filter_flag=PropertyFilterFlag.BASIC_FILTER, dtype=DataType.INTEGER,
                      description="""ID of the top-level parent.""")
halo.object_properties.add(host)

hostsub = ObjectProperty(property_name="hostsub", sort_flag=PropertySortFlag.BASIC_SORT,
                         filter_flag=PropertyFilterFlag.BASIC_FILTER, dtype=DataType.INTEGER,
                         description="""ID of the direct parent.""")
halo.object_properties.add(hostsub)


nbsub = ObjectProperty(property_name="nbsub", sort_flag=PropertySortFlag.BASIC_SORT,
                       filter_flag=PropertyFilterFlag.BASIC_FILTER, dtype=DataType.INTEGER,
                       description="""Number of sub-structures of the object.""")
halo.object_properties.add(nbsub)

# Particle count object property group
part_count = ObjectPropertyGroup(group_name="halo particle count", description="Number of particles in the object.")
halo.property_groups.add(part_count)
nbpart = ObjectProperty(property_name="nbpart", sort_flag=PropertySortFlag.BASIC_SORT,
                        filter_flag=PropertyFilterFlag.BASIC_FILTER, dtype=DataType.INTEGER,
                        description="""Number of particles in the object (DM+stars).""")
halo.object_properties.add(nbpart)
part_count.group_properties.add(nbpart)

nDM = ObjectProperty(property_name="nDM", sort_flag=PropertySortFlag.BASIC_SORT,
                     filter_flag=PropertyFilterFlag.BASIC_FILTER, dtype=DataType.INTEGER,
                     description="""Number of DM particles in the object.""")
halo.object_properties.add(nDM)
part_count.group_properties.add(nDM)

nstar = ObjectProperty(property_name="nstar", sort_flag=PropertySortFlag.BASIC_SORT,
                       filter_flag=PropertyFilterFlag.BASIC_FILTER, dtype=DataType.INTEGER,
                       description="""Number of star particles in the object.""")
halo.object_properties.add(nstar)
part_count.group_properties.add(nstar)

# Total particle count object property group
tot_part_count = ObjectPropertyGroup(group_name="total halo particle count",
                                     description="Total number of particles in the halo and its substructures.")
halo.property_groups.add(tot_part_count)
ntot = ObjectProperty(property_name="ntot", sort_flag=PropertySortFlag.BASIC_SORT,
                      filter_flag=PropertyFilterFlag.BASIC_FILTER, dtype=DataType.INTEGER,
                      description="""Total number of particles in the object and its substructures (DM + stars).""")
halo.object_properties.add(ntot)
tot_part_count.group_properties.add(ntot)

ntotDM = ObjectProperty(property_name="ntotDM", sort_flag=PropertySortFlag.BASIC_SORT,
                        filter_flag=PropertyFilterFlag.BASIC_FILTER, dtype=DataType.INTEGER,
                        description="""Total number of DM particles in the object and its substructures.""")
halo.object_properties.add(ntotDM)
tot_part_count.group_properties.add(ntotDM)

ntotstar = ObjectProperty(property_name="ntotstar", sort_flag=PropertySortFlag.BASIC_SORT,
                          filter_flag=PropertyFilterFlag.BASIC_FILTER, dtype=DataType.INTEGER,
                          description="""Total number of star particles in the object and its substructures.""")
halo.object_properties.add(ntotstar)
tot_part_count.group_properties.add(ntotstar)

# Total mass of the object
mtot = ObjectPropertyGroup(group_name="total halo mass",
                           description="Total mass of the particles within the object.")
halo.property_groups.add(mtot)
m = ObjectProperty(property_name="m", sort_flag=PropertySortFlag.BASIC_SORT,
                   filter_flag=PropertyFilterFlag.BASIC_FILTER, dtype=DataType.REAL, unit=U.Msun,
                   description="""Total mass of the particles within the object (DM + stars).""")
halo.object_properties.add(m)
tot_part_count.group_properties.add(m)

mDM = ObjectProperty(property_name="mDM", sort_flag=PropertySortFlag.BASIC_SORT,
                     filter_flag=PropertyFilterFlag.BASIC_FILTER, dtype=DataType.REAL, unit=U.Msun,
                     description="""Total mass of the DM particles within the object.""")
halo.object_properties.add(mDM)
tot_part_count.group_properties.add(mDM)

mstar = ObjectProperty(property_name="mstar", sort_flag=PropertySortFlag.BASIC_SORT,
                       filter_flag=PropertyFilterFlag.BASIC_FILTER, dtype=DataType.REAL, unit=U.Msun,
                       description="""Total mass of the star particles within the object.""")
halo.object_properties.add(mstar)
tot_part_count.group_properties.add(mstar)

# TODO : mtot, mtotDM, mtotstar, mvir, mbulge, ...

# Kinetic energy
# Custom unit here to describe
eku = U.Unit.create_unit(name="Msun_km2_s2", base_unit=U.Msun*(U.km/U.s)**2, descr="Solar mass times square kilometer per second",
                         latex="\\textrm{M}_{\\odot}\\cdot(\\textrm{km}/\\textrm{s})^{2}")
ek = ObjectProperty(property_name="ek", sort_flag=PropertySortFlag.BASIC_SORT, unit=eku,
                    filter_flag=PropertyFilterFlag.BASIC_FILTER, dtype=DataType.REAL,
                    description="""Kinetic energy of the halo.""")
halo.object_properties.add(ek)

# TODO other properties





# -------------------------------------------------- Catalog definition ---------------------------------------------- #
# Target object catalog + field value arrays
cat = sn.catalogs.add(Catalog(target_object=halo, name="Halo catalog at $z \simeq 3.0$",
                              description="Full description of my catalog containing 56285 haloes."))

# TODO Put your catalog values here, property by property (set the 'values' attribute)
f_id_halo = cat.catalog_fields.add(CatalogField(obj_prop=id_halo, values=N.arange(1, 56286)))
f_mDM = cat.catalog_fields.add(CatalogField(obj_prop=mDM, values=N.random.uniform(size=56285)))
f_m = cat.catalog_fields.add(CatalogField(obj_prop=m, values=N.random.uniform(size=56285)))
f_mstar = cat.catalog_fields.add(CatalogField(obj_prop=mstar, values=N.random.uniform(size=56285)))
f_ek = cat.catalog_fields.add(CatalogField(obj_prop=ek, values=N.random.uniform(size=56285)))


# Add datafiles to the catalog if required.
# cdf1 = cat.datafiles.add(Datafile(name="My datafile", description="---"))
# cdf1[FileType.PNG_FILE] = os.path.join("/data", "io", "datafiles", "plot_image_IMF.png")
# cdf2 = cat.datafiles.add(Datafile(name="My datafile (2)", description="---"))
# cdf2[FileType.JPEG_FILE] = os.path.join("/data", "io", "datafiles", "plot_with_legend.jpg")
# -------------------------------------------------------------------------------------------------------------------- #


# ---------------------------------------------- Data processing services bindings ----------------------------------- #
# Add data processing services to a snapshot
# dps1 = sn.processing_services.add((DataProcessingService(service_name="column_density_map",
#                                                          data_host="MyDepartmentCluster")))

# Define catalog field bindings to automatically fill the data processing service parameter value 'pv' with a catalog
# field value 'fv' of one of your catalog's object according to the formula : pv = scale * fv + offset. Default scale
# value is 1.0 and default offset is 0.0.
# fbx = dps1.catalog_field_bindings.add(CatalogFieldBinding(catalog_field=fx, param_key="x",
#                                                           scale=1.0e2, offset=-50.0))
# fby = dps1.catalog_field_bindings.add(CatalogFieldBinding(catalog_field=fy, param_key="y",
#                                                           scale=1.0e2, offset=-50.0))
# fbz = dps1.catalog_field_bindings.add(CatalogFieldBinding(catalog_field=fz, param_key="z",
#                                                           scale=1.0e2, offset=-50.0))
# -------------------------------------------------------------------------------------------------------------------- #


# Save study in HDF5 file
study.save_HDF5("./obelisk_study.h5", galactica_checks=True)

# Eventually reload it from HDF5 file to edit its content
# study = SimulationStudy.load_HDF5("./obelisk_study.h5")
