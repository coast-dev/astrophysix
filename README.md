Astrophysix package
===================

[![Documentation Status](https://readthedocs.org/projects/astrophysix/badge/?version=latest)](https://astrophysix.readthedocs.io/en/latest/?badge=latest)

The ``astrophysix`` package is designed for computational astrophysicists who wish to document their numerical projects in a simple, portable, user-friendly way. For more detailed information, see the [full documentation](https://astrophysix.readthedocs.io/en/latest/).